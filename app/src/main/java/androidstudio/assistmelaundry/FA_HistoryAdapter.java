package androidstudio.assistmelaundry;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by JP on 11/27/2017.
 */

public class FA_HistoryAdapter extends BaseAdapter {
    ArrayList<C_BookingClass> items;
    Context c;

    public FA_HistoryAdapter(Context c, ArrayList<C_BookingClass> items) {
        this.c = c;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(c).inflate(R.layout.orderhistoryrow, viewGroup, false);
        }
        Typeface headerfont = Typeface.createFromAsset(c.getAssets(),  "fonts/Poppins-Bold.otf");
        Typeface subheaderfont = Typeface.createFromAsset(c.getAssets(),  "fonts/Poppins-Medium.otf");
        Typeface regulartextfont = Typeface.createFromAsset(c.getAssets(),  "fonts/Poppins-Regular.otf");

        final C_BookingClass o = (C_BookingClass) this.getItem(i);
        TextView tvdate = (TextView) view.findViewById(R.id.orderdate);
        TextView tvtime = (TextView) view.findViewById(R.id.ordertime);
        TextView tvservice = (TextView) view.findViewById(R.id.orderservice);
        TextView tvservicehldr = (TextView) view.findViewById(R.id.orderservicehldr);
        TextView tvid = (TextView) view.findViewById(R.id.orderid);
        TextView tvidhldr = (TextView) view.findViewById(R.id.orderidhldr);

        tvdate.setTypeface(headerfont);
        tvtime.setTypeface(headerfont);
        tvservice.setTypeface(regulartextfont);
        tvservicehldr.setTypeface(regulartextfont);
        tvid.setTypeface(regulartextfont);
        tvidhldr.setTypeface(regulartextfont);

        if(o.getBookingid() != null && o.getBookingservice() != null && o.getBookingtimestamp() != null) {
            String valhldr = o.getBookingtimestamp();
            String stringtoparse = valhldr;
            String parser = stringtoparse;
            String delims = "[/]+";
            String[] tokens = parser.split(delims);
            String date = String.valueOf(tokens[0]);
            String time = String.valueOf(tokens[1]);

            tvdate.setText(date);
            tvtime.setText(time);
            tvservicehldr.setText(o.getBookingservice());
            tvidhldr.setText(o.getBookingid());
        }
        return view;
    }
}