package androidstudio.assistmelaundry;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by JP on 12/6/2017.
 */

public class BR_CurrentOrdersFragment extends android.support.v4.app.Fragment {

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    public String uid,orderid,orderdate,address,contact,orderservice, total, orderstatus, orderiderid;
    public String orderpowder, orderpowdertotal, orderfabcon, orderfabcontotal, orderoveralltotal, ordercharge, orderkilocount, orderotherinfo,orderminkgcount;
    public String USER_TYPE;
    public String ordertransaction;
    C_BookingClass lm;
    ArrayList<C_BookingClass> listmenu;
    ArrayList<String> bookdetails;
    FA_HistoryAdapter itemAdapter;

    boolean GPSStatus;
    LocationManager locationManager;

    //UI
    @BindView(R.id.clienthistorylist)
    ListView orderlist;
    @BindView(R.id.header)
    TextView header;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookridercurrentorders, container, false);
        ButterKnife.bind(this, view);
        InitializeDesign();
        InitializeFunctions();
        return view;
    }

    public void InitializeDesign(){
        Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Bold.otf");
        header.setTypeface(headerfont);
    }

    public void InitializeFunctions(){
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        lm = new C_BookingClass();
        listmenu = new ArrayList<>();
        bookdetails = new ArrayList<>();
        RetrieveTransactionStatus();
        RetrieveBookingHistory();

        itemAdapter = new FA_HistoryAdapter(getActivity(), listmenu);
        orderlist.setAdapter(itemAdapter);

        orderlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                String valhldr = bookdetails.get(position);
                String stringtoparse = valhldr;
                String parser = stringtoparse;
                String delims = "[<>]+";
                String[] tokens = parser.split(delims);
                final String ordid = String.valueOf(tokens[0]);
                String orddate = String.valueOf(tokens[1]);
                String ordadd = String.valueOf(tokens[2]);
                String ordcon = String.valueOf(tokens[3]);
                String ordtotal = String.valueOf(tokens[4]);
                String ordservice = String.valueOf(tokens[5]);
                String ordstatus = String.valueOf(tokens[6]);
                String ordpowder = String.valueOf(tokens[7]);
                String ordpowdertotal = String.valueOf(tokens[8]);
                String ordfabcon = String.valueOf(tokens[9]);
                String ordfabcontotal = String.valueOf(tokens[10]);
                String ordoveralltotal = String.valueOf(tokens[11]);
                String orddelcharge = String.valueOf(tokens[12]);
                String ordkilocount = String.valueOf(tokens[13]);
                String ordotherinfo = String.valueOf(tokens[14]);
                String ordminkgcount = String.valueOf(tokens[15]);

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_bookinginfocurrent, null);
                dialogBuilder.setView(dialogView);

                final AlertDialog alertDialog = dialogBuilder.create();
                Window window = alertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                final TextView tvordheader = (TextView) dialogView.findViewById(R.id.tvheader);
                final TextView tvordernum = (TextView) dialogView.findViewById(R.id.delordernum);
                final TextView tvordtotal = (TextView) dialogView.findViewById(R.id.deltotal);
                final TextView tvordstatus = (TextView) dialogView.findViewById(R.id.delstatus);
                final TextView tvordaddress = (TextView) dialogView.findViewById(R.id.deladdress);
                final TextView tvordcontact = (TextView) dialogView.findViewById(R.id.delcontact);
                final TextView tvordchange = (TextView) dialogView.findViewById(R.id.delchangefor);
                final TextView tvordtimestamp = (TextView) dialogView.findViewById(R.id.deltimestamp);
                final TextView tvordpowder = (TextView) dialogView.findViewById(R.id.delpowder);
                final TextView tvordfabcon = (TextView) dialogView.findViewById(R.id.delfabcon);
                final TextView tvordkilocount = (TextView) dialogView.findViewById(R.id.delservicekilo);
                final TextView tvordotherinfo = (TextView) dialogView.findViewById(R.id.delotherinfo);

                Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Bold.otf");
                Typeface subheaderfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Medium.otf");
                Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Regular.otf");

                tvordheader.setTypeface(headerfont);
                tvordstatus.setTypeface(subheaderfont);
                tvordernum.setTypeface(regulartextfont);
                tvordtotal.setTypeface(regulartextfont);
                tvordaddress.setTypeface(regulartextfont);
                tvordcontact.setTypeface(regulartextfont);
                tvordchange.setTypeface(regulartextfont);
                tvordtimestamp.setTypeface(regulartextfont);
                tvordpowder.setTypeface(regulartextfont);
                tvordfabcon.setTypeface(regulartextfont);
                tvordkilocount.setTypeface(regulartextfont);
                tvordotherinfo.setTypeface(regulartextfont);

                FancyButton cancel_btn = (FancyButton) dialogView.findViewById(R.id.buttonclose);
                FancyButton deliver_btn = (FancyButton) dialogView.findViewById(R.id.buttonDeliver);

                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.hide();
                    }
                });

                deliver_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(ordertransaction.equals("false")) {
                            CheckGpsStatus();
                            if(GPSStatus == true){
                                alertDialog.dismiss();
                                SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                editor.putString("booktransaction", "deliverytrue");
                                editor.putString("bookid", ordid);
                                editor.apply();

                                mDatabase.child("Bookings").child(ordid).child("bookingstatus").setValue("delivery");

                                Fragment fragment = new BR_CurrentDeliveryFragment();
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.contentContainer, fragment);
                                fragmentTransaction.commit();
                            }else{
                                displayPromptForEnablingGPS(getActivity());
                            }
                        }
                        else if ((ordertransaction.equals("true")) || (ordertransaction.equals("deliverytrue"))){
                            Toast.makeText(getActivity(), "You have a current transaction. Cannot proceed with deliveries", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                tvordernum.setText("Booking ID: "+ordid);
                tvordstatus.setText("Booking Status: "+ordstatus);
                tvordaddress.setText("Booking Address: "+ordadd);
                tvordcontact.setText("Contact Number: "+ordcon);
                tvordchange.setText("Service Requested: "+ordservice);
                tvordkilocount.setText("Service Kilo Count: "+ordkilocount);
                tvordpowder.setText("Washing Powder Requested: "+ordpowder);
                tvordfabcon.setText("Fabric Conditioner Requested: "+ordfabcon);
                tvordotherinfo.setText("Other Services/Information: "+ordotherinfo);
                tvordtimestamp.setText("Delivery Timestamp: "+orddate);
                displayFinalTotal(ordminkgcount, ordtotal,ordpowdertotal,ordfabcontotal,ordoveralltotal,orddelcharge,tvordtotal);

                alertDialog.show();
            }});
    }

    public void RetrieveBookingHistory() {
        C_FirebaseClass firebaseFunctions = new C_FirebaseClass(getActivity());
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        mRootRef.child("Bookings")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        final C_BookingClass book = dataSnapshot.getValue(C_BookingClass.class);
                        uid = book.getUid();
                        orderid = book.getBookingid();
                        address = book.getBookingaddress();
                        contact = book.getBookingcontact();
                        total = book.getBookingtotal();
                        orderdate = book.getBookingtimestamp();
                        orderservice = book.getBookingservice();
                        orderstatus = book.getBookingstatus();
                        orderiderid = book.getRiderid();
                        orderpowder = book.getBookingpowder();
                        orderpowdertotal = book.getBookingpowdertotal();
                        orderfabcon = book.getBookingfabcon();
                        orderfabcontotal = book.getBookingfabcontotal();
                        orderoveralltotal = book.getBookingoveralltotal();
                        ordercharge = book.getBookingcharge();
                        orderkilocount = book.getBookingkilocount();
                        orderotherinfo = book.getBookingotherservices();
                        orderminkgcount = book.getBranchminkgcount();

                        if (uid != null && orderid != null && address != null && contact != null && total != null && orderdate != null
                                && orderservice != null && orderstatus != null && orderiderid != null && orderpowder != null && orderpowdertotal != null && orderfabcon != null
                                && orderfabcontotal != null && orderoveralltotal != null && ordercharge != null && orderkilocount != null && orderotherinfo != null && orderminkgcount != null) {
                            if (mAuth.getCurrentUser().getUid().equals(orderiderid)) {
                                if (orderstatus.equals("orderprocessed")) {
                                    lm = new C_BookingClass();
                                    lm.setBookingid(orderid);
                                    lm.setBookingtimestamp(orderdate);
                                    lm.setBookingaddress(address);
                                    lm.setBookingcontact(contact);
                                    lm.setBookingtotal(total);
                                    lm.setBookingservice(orderservice);
                                    lm.setBookingpowder(orderpowder);
                                    lm.setBookingpowdertotal(orderpowdertotal);
                                    lm.setBookingfabcon(orderfabcon);
                                    lm.setBookingfabcontotal(orderfabcontotal);
                                    lm.setBookingoveralltotal(orderoveralltotal);
                                    lm.setBookingcharge(ordercharge);
                                    lm.setBookingkilocount(orderkilocount);
                                    lm.setBookingotherservices(orderotherinfo);
                                    lm.setBranchminkgcount(orderminkgcount);
                                    listmenu.add(lm);
                                    itemAdapter.notifyDataSetChanged();
                                    bookdetails.add(orderid + "<>" + orderdate + "<>" + address + "<>" + contact + "<>" + total + "<>"
                                            + orderservice + "<>" + orderstatus + "<>" + orderpowder + "<>" + orderpowdertotal + "<>"
                                            + orderfabcon + "<>" + orderfabcontotal + "<>" + orderoveralltotal + "<>" + ordercharge + "<>"
                                            + orderkilocount + "<>" + orderotherinfo + "<>" + orderminkgcount);
                                }
                            }
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public String displayFinalTotal(String minkgcount, String finaltotal, String finalpowdertotal, String finalfabcontotal, String finaloveralltotal, String finalcharge, TextView displaytotal){
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString str1= new SpannableString("Service Total(Per "+minkgcount+" Kilos): ");
        str1.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str1.length(), 0);
        builder.append(str1);

        SpannableString str2= new SpannableString( "₱"+finaltotal+".00"+"\n");
        str2.setSpan(new ForegroundColorSpan(Color.RED), 0, str2.length(), 0);
        builder.append(str2);

        SpannableString str3= new SpannableString("Powder Price Total: ");
        str3.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str3.length(), 0);
        builder.append(str3);

        SpannableString str4= new SpannableString( "₱"+finalpowdertotal+".00"+"\n");
        str4.setSpan(new ForegroundColorSpan(Color.RED), 0, str4.length(), 0);
        builder.append(str4);

        SpannableString str5= new SpannableString("FabCon Price Total: ");
        str5.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str5.length(), 0);
        builder.append(str5);

        SpannableString str6= new SpannableString( "₱"+finalfabcontotal+".00"+"\n");
        str6.setSpan(new ForegroundColorSpan(Color.RED), 0, str6.length(), 0);
        builder.append(str6);

        SpannableString str7= new SpannableString("Service Charge: ");
        str7.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str7.length(), 0);
        builder.append(str7);

        SpannableString str8= new SpannableString( "₱"+finalcharge+".00"+"\n");
        str8.setSpan(new ForegroundColorSpan(Color.RED), 0, str8.length(), 0);
        builder.append(str8);

        SpannableString str9= new SpannableString("Approximate Final Payout: ");
        str9.setSpan(new StyleSpan(Typeface.BOLD), 0, str9.length(), 0);
        str9.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str9.length(), 0);
        builder.append(str9);

        SpannableString str10= new SpannableString( "₱"+finaloveralltotal+".00");
        str10.setSpan(new StyleSpan(Typeface.BOLD), 0, str10.length(), 0);
        str10.setSpan(new ForegroundColorSpan(Color.RED), 0, str10.length(), 0);
        builder.append(str10);
        displaytotal.setText(builder, TextView.BufferType.SPANNABLE);

        return "Final Total";
    }

    public void RetrieveTransactionStatus(){
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        ordertransaction = prefs.getString("booktransaction", "order");
    }

    public void CheckGpsStatus(){
        locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        GPSStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static void displayPromptForEnablingGPS(final Activity activity) {
        final AlertDialog.Builder builder =  new AlertDialog.Builder(activity);
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        final String message = "GPS/Location Setting must be enabled to continue";
        Toast.makeText(activity, "Location Settings is not On!", Toast.LENGTH_SHORT).show();
        builder.setMessage(message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                activity.startActivity(new Intent(action));
                                d.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });
        builder.create().show();
    }
}

