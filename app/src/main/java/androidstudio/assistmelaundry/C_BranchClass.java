package androidstudio.assistmelaundry;

import java.util.ArrayList;

/**
 * Created by JP on 3/21/2018.
 */

public class C_BranchClass {
    private String LaundryName;
    private String Address;
    private String ContactNo;
    private String ContactPerson;
    private String LaundryShop;
    private String Status;
    private String ServiceFee;

    private Double Lat;
    private Double Lng;

    private int MinWeight;

    private ArrayList <String> Service;
    private ArrayList <String> Fabcon;
    private ArrayList <String> Powder;

    public C_BranchClass(){}

    public C_BranchClass(String LaundryName, String Address, String ContactNo, String ContactPerson, String LaundryShop,
                         String Status, Double Lat, Double Lng, int MinWeight,
                         ArrayList Service, ArrayList Fabcon, ArrayList Powder, String ServiceFee) {
        this.LaundryName = LaundryName;
        this.Address = Address;
        this.ContactNo = ContactNo;
        this.ContactPerson = ContactPerson;
        this.LaundryShop = LaundryShop;
        this.Status = Status;
        this.Lat = Lat;
        this.Lng = Lng;
        this.MinWeight = MinWeight;
        this.Service = Service;
        this.Fabcon = Fabcon;
        this.Powder = Powder;
        this.ServiceFee = ServiceFee;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String ContactNo) {
        this.ContactNo = ContactNo;
    }

    public String getContactPerson() {
        return ContactPerson;
    }

    public void setContactPerson(String ContactPerson) {
        this.ContactPerson = ContactPerson;
    }

    public String getLaundryShop() {
        return LaundryShop;
    }

    public void setLaundryShop(String LaundryShop) {
        this.LaundryShop = LaundryShop;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public Double getLat() {
        return Lat;
    }

    public void setLat(Double Lat) {
        this.Lat = Lat;
    }

    public Double getLng() {
        return Lng;
    }

    public void setLng(Double Lng) {
        this.Lng = Lng;
    }

    public int getMinWeight() {
        return MinWeight;
    }

    public void setMinWeight(int MinWeight) {
        this.MinWeight = MinWeight;
    }

    public String getLaundryName() {
        return LaundryName;
    }

    public void setLaundryName(String LaundryName) {
        this.LaundryName = LaundryName;
    }

    public ArrayList<String> getService() {
        return Service;
    }

    public void setService(ArrayList<String> service) {
        Service = service;
    }

    public String getServiceFee() {
        return ServiceFee;
    }

    public void setServiceFee(String serviceFee) {
        ServiceFee = serviceFee;
    }

    public ArrayList<String> getFabcon() {
        return Fabcon;
    }

    public void setFabcon(ArrayList<String> fabcon) {
        Fabcon = fabcon;
    }

    public ArrayList<String> getPowder() {
        return Powder;
    }

    public void setPowder(ArrayList<String> powder) {
        Powder = powder;
    }
}
