package androidstudio.assistmelaundry;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by JP on 12/6/2017.
 */

public class BR_CurrentDeliveryFragment extends android.support.v4.app.Fragment {

    //Preference
    SharedPreferences.Editor editor;
    private String USER_TYPE;
    String orderid, ordername, orderaddress, ordercontact, ordertimestamp, orderlat, orderlong, orderstat, orderservice;
    String orderpowder, orderpowdertotal, orderfabcon, orderfabcontotal, orderoveralltotal, ordercharge, orderkilocount, orderotherinfo, orderminkgcount;

    //Location Variables
    LocationManager mLocationManager;
    String latitude, longitude;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Order Completion
    String ordertotal = "0";

    //UI
    MaterialDialog dialog;
    MaterialDialog finaldialog;
    @BindView(R.id.tvheader)
    TextView tvheader;
    @BindView(R.id.delordernum)
    TextView tvorderid;
    @BindView(R.id.delcustname)
    TextView tvordername;
    @BindView(R.id.deladdress)
    TextView tvorderaddress;
    @BindView(R.id.delcontact)
    TextView tvordercontact;
    @BindView(R.id.delservice)
    TextView tvorderservice;
    @BindView(R.id.delstatus)
    TextView tvorderstatus;
    @BindView(R.id.deltimestamp)
    TextView tvordertimestamp;
    @BindView(R.id.deltotal)
    TextView tvordertotal;
    @BindView(R.id.delpowder)
    TextView tvorderpowder;
    @BindView(R.id.delfabcon)
    TextView tvorderfabcon;
    @BindView(R.id.buttongetdirections)
    FancyButton btnGetDirections;
    @BindView(R.id.buttonfinishorder)
    FancyButton btnFinish;
    @BindView(R.id.btn_call)
    CircleImageView btnCall;
    @BindView(R.id.btn_text)
    CircleImageView btnText;
    @BindView(R.id.tvcontactrider)
    TextView tvcontact;
    @BindView(R.id.delservicekilo)
    TextView tvserviceskilo;
    @BindView(R.id.delotherinfo)
    TextView tvotherinfo;
    @BindView(R.id.tvserviceheader)
    TextView tvserviceheader;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookridercurrentdelivery, container, false);
        ButterKnife.bind(this, view);
        InitializeDesign();
        InitializeFunctions();
        return view;
    }

    public void InitializeDesign(){
        Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Bold.otf");
        Typeface subheaderfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Medium.otf");
        Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Regular.otf");

        tvheader.setTypeface(headerfont);
        tvorderid.setTypeface(regulartextfont);
        tvorderstatus.setTypeface(subheaderfont);
        tvordercontact.setTypeface(regulartextfont);
        tvorderaddress.setTypeface(regulartextfont);
        tvorderservice.setTypeface(regulartextfont);
        tvorderpowder.setTypeface(regulartextfont);
        tvorderfabcon.setTypeface(regulartextfont);
        tvordertimestamp.setTypeface(regulartextfont);
        tvordertotal.setTypeface(regulartextfont);
        tvcontact.setTypeface(headerfont);
        tvserviceskilo.setTypeface(regulartextfont);
        tvotherinfo.setTypeface(regulartextfont);
        tvserviceheader.setTypeface(headerfont);
    }


    public void InitializeFunctions() {
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        RetrieveBookingData();
        getLastKnownLocation();
        onChangeLocation();

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", ordercontact, null));
                startActivity(intent);
            }
        });

        btnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", ordercontact, null)));
            }
        });

        btnGetDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+latitude+","+longitude+"&daddr="+orderlat+","+orderlong));
                startActivity(intent);

            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // <--------------------------------------------FOR DELIVERY ARRIVAL-------------------------------------------->
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_choice, null);
                dialogBuilder.setView(dialogView);

                final AlertDialog OptionalertarrivalDialog = dialogBuilder.create();
                Window window = OptionalertarrivalDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Bold.otf");

                optionheader.setTypeface(headerfont);
                optionheader.setText("Arrived at Customer Location?");

                negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        OptionalertarrivalDialog.dismiss();
                    }
                });

                positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // <--------------------------------------------FOR DELIVERY BACK 2 CUSTOMER-------------------------------------------->
                        mDatabase.child("Bookings").child(orderid).child("bookingstatus").setValue("deliverarrived");
                        OptionalertarrivalDialog.dismiss();
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.popup_choice, null);
                        dialogBuilder.setView(dialogView);

                        final AlertDialog OptionalertDialog = dialogBuilder.create();
                        Window window = OptionalertDialog.getWindow();
                        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        window.setGravity(Gravity.CENTER);

                        final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                        final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                        final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                        optionheader.setText("Finish Booking?");

                        negative.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                OptionalertDialog.dismiss();
                            }
                        });

                        positive.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finaldialog = new MaterialDialog.Builder(getActivity())
                                        .title("Successful!")
                                        .content("Order was successfully done")
                                        .positiveText("Close")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                OptionalertDialog.dismiss();
                                                mDatabase.child("Bookings").child(orderid).child("bookingstatus").setValue("delivered");
                                                SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                                editor.putString("booktransaction", "false");
                                                editor.putString("bookid", "none");
                                                editor.putString(orderid+"bookname", "none");
                                                editor.putString(orderid+"bookaddress", "none");
                                                editor.putString(orderid+"bookcontact", "none");
                                                editor.putString(orderid+"booktimestamp", "none");
                                                editor.putString(orderid+"booktotal", "none");
                                                editor.putString(orderid+"booklatitude", "none");
                                                editor.putString(orderid+"booklongitude", "none");
                                                editor.putString(orderid+"bookservice", "none");
                                                editor.putString(orderid+"bookpowder", "none");
                                                editor.putString(orderid+"bookpowdertotal", "none");
                                                editor.putString(orderid+"bookfabcon", "none");
                                                editor.putString(orderid+"bookfabcontotal", "none");
                                                editor.putString(orderid+"bookoveralltotal", "none");
                                                editor.putString(orderid+"bookcharge", "none");
                                                editor.putString(orderid + "bookingotherservices", "none");
                                                editor.putString(orderid + "bookingkilocount", "none");
                                                editor.putString(orderid + "branchminkgcount", "none");
                                                editor.apply();

                                                finaldialog.dismiss();

                                                Fragment fragment = new BR_MainFragment();
                                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                fragmentTransaction.remove(new BR_CurrentTransFragment());
                                                fragmentTransaction.replace(R.id.contentContainer, fragment);
                                                fragmentTransaction.commit();
                                            }
                                        })
                                        .show();
                            }
                        });
                        OptionalertDialog.show();
                    }
                });

                OptionalertarrivalDialog.show();
            }
        });
    }

    public void RetrieveBookingData() {
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        orderid = prefs.getString("bookid", "ID");
        ordername = prefs.getString(orderid+"bookcustname", "Name");
        orderaddress = prefs.getString(orderid+"bookaddress", "Address");
        ordercontact = prefs.getString(orderid+"bookcontact", "Contact");
        ordertimestamp = prefs.getString(orderid+"booktimestamp", "Timestamp");
        orderlat = prefs.getString(orderid+"booklatitude", "Latitude");
        orderlong = prefs.getString(orderid+"booklongitude", "Longitude");
        orderstat = prefs.getString(orderid+"bookstatus", "Status");
        ordercharge = prefs.getString(orderid+"bookcharge", "Delivery Charge");
        orderservice = prefs.getString(orderid+"bookservice", "Service");
        orderpowder = prefs.getString(orderid+"bookpowder", "Powder");
        orderfabcon = prefs.getString(orderid+"bookfabcon", "Fabcon");
        orderpowdertotal = prefs.getString(orderid+"bookpowdertotal", "PowderTotal");
        orderfabcontotal = prefs.getString(orderid+"bookfabcontotal", "FabconTotal");
        ordertotal = prefs.getString(orderid+"booktotal", "ServiceTotal");
        orderoveralltotal = prefs.getString(orderid+"bookoveralltotal", "OverallTotal");
        orderkilocount = prefs.getString(orderid+"bookingkilocount", "KiloCount");
        orderotherinfo = prefs.getString(orderid+"bookingotherservices", "OtherInfo");
        orderminkgcount = prefs.getString(orderid+"branchminkgcount", "MinKgCount");

        tvorderid.setText("Booking ID: " + orderid);
        tvordername.setText("Customer Name:" + ordername);
        tvordercontact.setText("Customer Contact: " + ordercontact);
        tvorderaddress.setText("Customer Address: " + orderaddress);
        tvordertimestamp.setText("Booking Timestamp: " + ordertimestamp);
        tvorderstatus.setText("Booking Status: " + orderstat);
        tvorderservice.setText("The Service requested: " + orderservice);
        tvserviceskilo.setText("Service Kilo Count: " + orderminkgcount);
        tvorderpowder.setText("Washing Powder requested: " + orderpowder);
        tvorderfabcon.setText("Fabric Conditioner requested: " + orderfabcon);
        tvotherinfo.setText("Other Services/Information: "+orderotherinfo);
        displayFinalTotal(orderminkgcount,ordertotal, orderpowdertotal, orderfabcontotal, ordercharge, orderoveralltotal, tvordertotal);
    }

    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission Check");
                Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
                return null;
            }
            else{
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    bestLocation = l;
                    latitude  = String.valueOf(bestLocation.getLatitude());
                    longitude = String.valueOf(bestLocation.getLongitude());

                    mDatabase.child("Bookings").child(orderid).child("driverlat").setValue(latitude);
                    mDatabase.child("Bookings").child(orderid).child("driverlong").setValue(longitude);
                }
            }

        }
        return bestLocation;
    }

    public void onChangeLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        {
            LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    getLastKnownLocation();
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {}

                @Override
                public void onProviderEnabled(String s) {}

                @Override
                public void onProviderDisabled(String s) {}
            };
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                    locationListener);
        }
    }

    public void displayFinalTotal(String minkgcount,String ordertotal, String powdertotal, String fabcontotal, String ordercharge, String overalltotal, TextView tvoveralltotal){
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString str1= new SpannableString("Service Total(Per "+minkgcount+" Kilos): ");
        str1.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str1.length(), 0);
        builder.append(str1);

        SpannableString str2= new SpannableString( "₱"+ordertotal+".00"+"\n");
        str2.setSpan(new ForegroundColorSpan(Color.RED), 0, str2.length(), 0);
        builder.append(str2);

        SpannableString str3= new SpannableString("Powder Price Total: ");
        str3.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str3.length(), 0);
        builder.append(str3);

        SpannableString str4= new SpannableString( "₱"+powdertotal+".00"+"\n");
        str4.setSpan(new ForegroundColorSpan(Color.RED), 0, str4.length(), 0);
        builder.append(str4);

        SpannableString str5= new SpannableString("FabCon Price Total: ");
        str5.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str5.length(), 0);
        builder.append(str5);

        SpannableString str6= new SpannableString( "₱"+fabcontotal+".00"+"\n");
        str6.setSpan(new ForegroundColorSpan(Color.RED), 0, str6.length(), 0);
        builder.append(str6);

        SpannableString str7= new SpannableString("Service Charge: ");
        str7.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str7.length(), 0);
        builder.append(str7);

        SpannableString str8= new SpannableString( "₱"+ordercharge+".00"+"\n");
        str8.setSpan(new ForegroundColorSpan(Color.RED), 0, str8.length(), 0);
        builder.append(str8);

        SpannableString str9= new SpannableString("Approximate Final Payout: ");
        str9.setSpan(new StyleSpan(Typeface.BOLD), 0, str9.length(), 0);
        str9.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str9.length(), 0);
        builder.append(str9);

        SpannableString str10= new SpannableString( "₱"+overalltotal+".00");
        str10.setSpan(new StyleSpan(Typeface.BOLD), 0, str10.length(), 0);
        str10.setSpan(new ForegroundColorSpan(Color.RED), 0, str10.length(), 0);
        builder.append(str10);
        tvoveralltotal.setText(builder, TextView.BufferType.SPANNABLE);
    }
}
