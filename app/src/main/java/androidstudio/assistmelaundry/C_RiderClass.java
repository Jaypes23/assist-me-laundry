package androidstudio.assistmelaundry;

/**
 * Created by JP on 4/5/2018.
 */

public class C_RiderClass {
    private String BranchId;
    private long ContactNo;
    private String Email;
    private String Id;
    private String LaundryShop;
    private String Name;
    private String Status;

    public C_RiderClass(){}

    public C_RiderClass(String BranchId, long ContactNo, String Email, String Id, String LaundryShop, String Name, String Status){
        this.BranchId = BranchId;
        this.ContactNo = ContactNo;
        this.Email = Email;
        this.Id = Id;
        this.LaundryShop = LaundryShop;
        this.Name = Name;
        this.Status = Status;
    }

    public String getBranchId() {
        return BranchId;
    }

    public void setBranchId(String branchId) {
        BranchId = branchId;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getLaundryShop() {
        return LaundryShop;
    }

    public void setLaundryShop(String laundryShop) {
        LaundryShop = laundryShop;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public long getContactNo() {
        return ContactNo;
    }

    public void setContactNo(long contactNo) {
        ContactNo = contactNo;
    }
}
