package androidstudio.assistmelaundry;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by JP on 12/6/2017.
 */

public class BR_CurrentTransFragment extends android.support.v4.app.Fragment {

    //Preference
    SharedPreferences.Editor editor;
    private String USER_TYPE;
    String bookingstatus;
    String orderid, ordername, orderaddress, ordercontact, ordertimestamp, orderlat, orderlong, orderstat;
    String ordercharge, orderotherinfo, orderkilocount;
    String orderbranch, orderbranchid, orderbranchmainid, orderbranchservicefee, orderbranchminkgcount;
    String rbranchid;

    //Location Variables
    LocationManager mLocationManager;
    String latitude, longitude;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Services
    int minkgcount;
    ArrayList<String> serviceshldr;
    ArrayList<String> fabconhldr;
    ArrayList<String> powderhldr;
    String stringtoparse, fstringtoparse, pstringtoparse;
    String parser, fparser, pparser;
    String delims, fdelims, pdelims;
    String[] tokens, ftokens, ptokens;


    //Order Completion
    String servicestatus = "false", kilostatus = "false", powderstatus = "false", fabconstatus = "false";
    String servicetotalhldr, powdertotalhldr, fabcontotalhldr;
    String powderitemhldr, fabconitemhldr;
    String ordertotal = "0",powdertotal = "0",fabcontotal = "0",overalltotal = "0";
    String otherservices;
    int powderquan = 0;
    int fabconquan = 0;
    String kilocounthldr;
    String kilocountstatus = "false";
    String kiloisdivisible = "false";
    String servicecostval;
    String kilocount;
    int kilocounttotal;

    //UI
    MaterialDialog dialog;
    MaterialDialog riderdialog;
    MaterialDialog finaldialog;
    MaterialDialog inputQuan;
    @BindView(R.id.header)
    TextView tvheader;
    @BindView(R.id.delordernum)
    TextView tvorderid;
    @BindView(R.id.delcustname)
    TextView tvordername;
    @BindView(R.id.deladdress)
    TextView tvorderaddress;
    @BindView (R.id.delcontact)
    TextView tvordercontact;
    @BindView(R.id.delservice)
    TextView tvorderservice;
    @BindView(R.id.delstatus)
    TextView tvorderstatus;
    @BindView(R.id.deltimestamp)
    TextView tvordertimestamp;
    @BindView(R.id.deltotal)
    TextView tvordertotal;
    @BindView(R.id.delpowder)
    TextView tvorderpowder;
    @BindView(R.id.delfabcon)
    TextView tvorderfabcon;
    @BindView(R.id.buttongetdirections)
    FancyButton btnGetDirections;
    @BindView(R.id.buttonfinishorder)
    FancyButton btnFinish;
    @BindView(R.id.btn_call)
    CircleImageView btnCall;
    @BindView(R.id.btn_text)
    CircleImageView btnText;
    @BindView(R.id.tvcontactrider)
    TextView tvcontact;
    @BindView(R.id.delotherinfo)
    TextView tvotherinfo;
    @BindView(R.id.delservicekilo)
    TextView tvkilocount;
    @BindView(R.id.tvserviceheader)
    TextView tvserviceheader;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookridercurrent, container, false);
        ButterKnife.bind(this, view);
        InitializeDesign();
        InitializeFunctions();
        return view;
    }

    public void InitializeDesign(){
        Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Bold.otf");
        Typeface subheaderfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Medium.otf");
        Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Regular.otf");

        tvheader.setTypeface(headerfont);
        tvorderid.setTypeface(regulartextfont);
        tvorderstatus.setTypeface(regulartextfont);
        tvordercontact.setTypeface(regulartextfont);
        tvorderaddress.setTypeface(regulartextfont);
        tvorderservice.setTypeface(regulartextfont);
        tvorderpowder.setTypeface(regulartextfont);
        tvorderfabcon.setTypeface(regulartextfont);
        tvordertimestamp.setTypeface(regulartextfont);
        tvordertotal.setTypeface(regulartextfont);
        tvcontact.setTypeface(headerfont);
        tvserviceheader.setTypeface(headerfont);
        tvotherinfo.setTypeface(regulartextfont);
        tvordername.setTypeface(regulartextfont);
        tvkilocount.setTypeface(regulartextfont);
    }

    public void InitializeFunctions() {
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        RetrieveBookingData();
        RetrieveBranchID();

        new RecheckBookingStatus().execute();
        new RetrieveBranchServices().execute();
        getLastKnownLocation();
        onChangeLocation();

        serviceshldr = new ArrayList<String>();
        powderhldr = new ArrayList<String>();
        fabconhldr = new ArrayList<String>();

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", ordercontact, null));
                startActivity(intent);
            }
        });

        btnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", ordercontact, null)));
            }
        });

        btnGetDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr="+latitude+","+longitude+"&daddr="+orderlat+","+orderlong));
                startActivity(intent);

            }
        });

        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // <--------------------------------------------FOR RIDER ARRIVAL-------------------------------------------->
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_choice, null);
                dialogBuilder.setView(dialogView);

                final AlertDialog OptionalertDialog = dialogBuilder.create();
                Window window = OptionalertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Bold.otf");

                optionheader.setTypeface(headerfont);
                optionheader.setText("Arrived at Customer Location?");

                negative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        OptionalertDialog.dismiss();
                    }
                });

                positive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MaterialDialog dialogarrival = new MaterialDialog.Builder(getActivity())
                                .title("Notification")
                                .content("You have arrived at customer location!")
                                .positiveText("Close")
                                .show();

                        OptionalertDialog.dismiss();
                        mDatabase.child("Bookings").child(orderid).child("bookingstatus").setValue("riderarrived");
                        btnFinish.setText("Details");
                        btnFinish.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // <--------------------------------------------FOR INPUTTING OF ORDER INFORMATION-------------------------------------------->
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                                LayoutInflater inflater = getActivity().getLayoutInflater();
                                View dialogView = inflater.inflate(R.layout.popup_completeorder, null);
                                dialogBuilder.setView(dialogView);

                                final AlertDialog CompleteOrderdialog = dialogBuilder.create();
                                Window window = CompleteOrderdialog.getWindow();
                                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                window.setGravity(Gravity.CENTER);

                                Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Bold.otf");
                                Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Medium.otf");

                                InitializeSpinners();

                                final TextView tvheader = (TextView) dialogView.findViewById(R.id.headertext);
                                final TextView tvinfoheader = (TextView) dialogView.findViewById(R.id.headerinfo);
                                final MaterialSpinner msspinner = (MaterialSpinner) dialogView.findViewById(R.id.servicespinner);
                                final TextInputLayout tlkilocount = (TextInputLayout) dialogView.findViewById(R.id.tl_kilocount);
                                final EditText etkilocount = (EditText) dialogView.findViewById(R.id.et_kilocount);
                                final MaterialSpinner powderspinner = (MaterialSpinner) dialogView.findViewById(R.id.servicepowder);
                                final MaterialSpinner fabconspinner = (MaterialSpinner) dialogView.findViewById(R.id.servicefabcon);
                                final TextView tvservicetotal = (TextView) dialogView.findViewById(R.id.servicetotal);
                                final TextView tvpowdertotal = (TextView) dialogView.findViewById(R.id.powdertotal);
                                final TextView tvfabcontotal = (TextView) dialogView.findViewById(R.id.fabcontotal);
                                final TextView tvoveralltotal = (TextView) dialogView.findViewById(R.id.overalltotal);
                                final FancyButton btnClose = (FancyButton) dialogView.findViewById(R.id.buttonClose);
                                final FancyButton btnFinal = (FancyButton) dialogView.findViewById(R.id.buttonFinalize);
                                final EditText etotherinfo = (EditText) dialogView.findViewById(R.id.etotherinfo);
                                final TextView tvselectservice = (TextView) dialogView.findViewById(R.id.tvselectservice);
                                final TextView tvselectpowder = (TextView) dialogView.findViewById(R.id.tvselectpowder);
                                final TextView tvselectfabcon = (TextView) dialogView.findViewById(R.id.tvselectconditioner);

                                tvheader.setTypeface(headerfont);
                                tvselectservice.setTypeface(headerfont);
                                tvselectpowder.setTypeface(headerfont);
                                tvselectfabcon.setTypeface(headerfont);
                                tvinfoheader.setTypeface(regulartextfont);
                                msspinner.setTypeface(regulartextfont);
                                powderspinner.setTypeface(regulartextfont);
                                fabconspinner.setTypeface(regulartextfont);
                                tvpowdertotal.setTypeface(regulartextfont);
                                tvservicetotal.setTypeface(regulartextfont);
                                tvfabcontotal.setTypeface(regulartextfont);
                                tvoveralltotal.setTypeface(regulartextfont);
                                etkilocount.setTypeface(regulartextfont);
                                tlkilocount.setTypeface(regulartextfont);
                                etotherinfo.setTypeface(regulartextfont);

                                etkilocount.setEnabled(false);

                                msspinner.setItems(serviceshldr);
                                msspinner.setSelectedIndex(serviceshldr.size()-1);
                                msspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                                            if(msspinner.getText().toString().equals("Select Service")){
                                                etkilocount.setEnabled(false);
                                                etkilocount.setText(String.valueOf(minkgcount));
                                                ordertotal = "0.00";
                                                servicetotalhldr = ordertotal;
                                                overalltotal = String.valueOf(Float.parseFloat(overalltotal) + Float.parseFloat(ordertotal));
                                                tvservicetotal.setText("Service Price(Per " + String.valueOf(minkgcount) + " Kilos): "+ordertotal);
                                                displayFinalTotal(tvoveralltotal);
                                            }else{
                                                etkilocount.setEnabled(true);
                                                etkilocount.setText(String.valueOf(minkgcount));
                                                if(servicestatus.equals("false")){
                                                    servicestatus = "true";
                                                }else{
                                                    overalltotal = String.valueOf(Float.parseFloat(overalltotal) - Float.parseFloat(servicetotalhldr));
                                                }

                                                stringtoparse = msspinner.getText().toString();
                                                parser = stringtoparse;
                                                delims = "[/]+";
                                                tokens = parser.split(delims);

                                                ordertotal = tokens[1];
                                                servicetotalhldr = ordertotal;
                                                overalltotal = String.valueOf(Float.parseFloat(overalltotal) + Float.parseFloat(ordertotal));
                                                tvservicetotal.setText("Service Price(Per " + String.valueOf(minkgcount) + " Kilos): "+ordertotal);
                                                displayFinalTotal(tvoveralltotal);
                                            }
                                    }
                                });

                                etkilocount.setText(String.valueOf(minkgcount));
                                etkilocount.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

                                    @Override
                                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

                                    @Override
                                    public void afterTextChanged(Editable editable) {
                                        if(!msspinner.getText().toString().equals("Select Service")) {
                                            servicecostval = ordertotal;
                                            kilocount = etkilocount.getText().toString().trim();
                                            kiloisdivisible = "false";

                                            if (kilocount.equals("") || kilocount.equals(" ")) {
                                                kilocountstatus = "false";
                                            } else if (Integer.valueOf(kilocount) < minkgcount) {
                                                kilocountstatus = "false";
                                                Toast.makeText(getActivity(), "Kilo Count is less than Branch Minimum", Toast.LENGTH_SHORT).show();
                                            } else {
                                                kilocountstatus = "true";

                                                if(kilostatus.equals("true")){
                                                    overalltotal = String.valueOf(Float.parseFloat(overalltotal) - Float.parseFloat(servicetotalhldr));
                                                    kilostatus = "false";
                                                }

                                                if (Integer.parseInt(kilocount) % minkgcount == 0) {
                                                    kilostatus = "true";
                                                    kilocounttotal = Integer.parseInt(kilocount) / minkgcount;
                                                    servicecostval = String.valueOf(Float.parseFloat(servicecostval) * kilocounttotal);
                                                    servicetotalhldr = servicecostval;
                                                    overalltotal = String.valueOf(Float.parseFloat(overalltotal) + Float.parseFloat(servicecostval));
                                                    tvservicetotal.setText("Service Price(Per " + String.valueOf(minkgcount) + " Kilos): " + servicecostval);
                                                    ordertotal = servicecostval;
                                                    displayFinalTotal(tvoveralltotal);
                                                } else {
                                                    while (kiloisdivisible.equals("false")) {
                                                        kilostatus = "true";
                                                        kilocount = String.valueOf(Integer.parseInt(kilocount) + 1);
                                                        if (Integer.parseInt(kilocount) % minkgcount == 0) {
                                                            kilocounttotal = Integer.parseInt(kilocount) / minkgcount;
                                                            servicecostval = String.valueOf(Float.parseFloat(servicecostval) * kilocounttotal);
                                                            servicetotalhldr = servicecostval;
                                                            overalltotal = String.valueOf(Float.parseFloat(overalltotal) + Float.parseFloat(servicecostval));
                                                            tvservicetotal.setText("Service Price(Per " + String.valueOf(minkgcount) + " Kilos): " + servicecostval);
                                                            ordertotal = servicecostval;
                                                            displayFinalTotal(tvoveralltotal);
                                                            kiloisdivisible = "true";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                });


                                powderspinner.setItems(powderhldr);
                                powderspinner.setSelectedIndex(powderhldr.size()-2);
                                powderspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
                                    @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                                        if(powderspinner.getText().toString().equals("Select Washing Powder")){
                                            powdertotal = "0";
                                            powderquan = 0;
                                            tvpowdertotal.setText("Powder Total: No Powder Selected");
                                            powdertotalhldr = powdertotal;
                                            displayFinalTotal(tvoveralltotal);
                                        }
                                        if(powderspinner.getText().toString().equals("User will provide Powder")){
                                            powdertotal = "0";
                                            powderquan = 0;
                                            tvpowdertotal.setText("Powder Provided");
                                            powdertotalhldr = powdertotal;
                                            displayFinalTotal(tvoveralltotal);
                                        }else{
                                            addPowder(tvpowdertotal, tvoveralltotal, powderspinner);

                                            tvpowdertotal.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    addPowder(tvpowdertotal, tvoveralltotal, powderspinner);
                                                }
                                            });
                                        }
                                    }
                                });

                                fabconspinner.setItems(fabconhldr);
                                fabconspinner.setSelectedIndex(fabconhldr.size()-2);
                                fabconspinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
                                    @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                                        if(fabconspinner.getText().toString().equals("Select Fabric Conditioner")){
                                            fabcontotal = "0";
                                            fabconquan = 0;
                                            tvfabcontotal.setText("Fab Con Total: No Fab Con Selected");
                                            fabcontotalhldr = fabcontotal;
                                            displayFinalTotal(tvoveralltotal);
                                        }
                                        if(fabconspinner.getText().toString().equals("User will provide Fabcon")){
                                            fabcontotal = "0";
                                            fabconquan = 0;
                                            tvfabcontotal.setText("Fabcon Provided");
                                            fabcontotalhldr = fabcontotal;
                                            displayFinalTotal(tvoveralltotal);
                                        }else{
                                            addFabcon(tvfabcontotal, tvoveralltotal, fabconspinner);

                                            tvfabcontotal.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    addFabcon(tvfabcontotal, tvoveralltotal, fabconspinner);
                                                }
                                            });
                                        }
                                    }
                                });


                                btnClose.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        for(int x=0;x<serviceshldr.size();x++){
                                            String hldr = serviceshldr.get(x);
                                            if(hldr.equals("Select Service")){
                                                serviceshldr.remove(x);
                                            }
                                        }

                                        for(int y=powderhldr.size()-1;y>0;y--){
                                            String phldr = powderhldr.get(y);
                                            if(phldr.equals("User will provide Powder") || phldr.equals("Select Washing Powder")){
                                                powderhldr.remove(y);
                                            }
                                        }
                                        
                                        for(int z=fabconhldr.size()-1;z>0;z--){
                                            String fhldr = fabconhldr.get(z);
                                            if(fhldr.equals("User will provide Fabcon") || fhldr.equals("Select Fabric Conditioner")){
                                                fabconhldr.remove(z);
                                            }
                                        }

                                        servicestatus = "false";
                                        kilostatus = "false";
                                        kilocountstatus = "false";
                                        powderstatus = "false";
                                        fabconstatus = "false";
                                        powdertotal = "0";
                                        powderquan = 0;
                                        fabcontotal = "0";
                                        fabconquan = 0;
                                        ordertotal = "0";
                                        overalltotal = "0";

                                        CompleteOrderdialog.dismiss();
                                    }
                                });

                                btnFinal.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        // <--------------------------------------------FOR FINAL PROCESSING OF ORDER-------------------------------------------->
                                        if (TextUtils.isEmpty(etkilocount.getText().toString())) {
                                            etkilocount.setError("Cannot be empty");
                                            return;
                                        }
                                        if(etkilocount.getText().toString().equals("0")){
                                            etkilocount.setError("Cannot be zero(0)");
                                            return;
                                        }
                                        if(kilocountstatus.equals("false")){
                                            if(etkilocount.getText().toString().trim().equals("") || etkilocount.getText().toString().trim().equals(" ")){
                                                etkilocount.setError("Kilo Count can't be empty");
                                                return;
                                            }
                                            if(Integer.parseInt(etkilocount.getText().toString()) < minkgcount){
                                                etkilocount.setError("Cannot be less than " + minkgcount);
                                                return;
                                            }
                                        }
                                        else {
                                            CompleteOrderdialog.dismiss();
                                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                                            LayoutInflater inflater = getActivity().getLayoutInflater();
                                            View dialogView = inflater.inflate(R.layout.popup_choice, null);
                                            dialogBuilder.setView(dialogView);

                                            final AlertDialog FinalOptionalertDialog = dialogBuilder.create();
                                            Window window = FinalOptionalertDialog.getWindow();
                                            window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                            window.setGravity(Gravity.CENTER);

                                            final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                                            final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                                            final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                                            Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Poppins-Bold.otf");

                                            optionheader.setTypeface(headerfont);
                                            optionheader.setText("Process Order?");

                                            negative.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    FinalOptionalertDialog.dismiss();
                                                }
                                            });

                                            positive.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    FinalOptionalertDialog.dismiss();
                                                    dialog = new MaterialDialog.Builder(getActivity())
                                                            .title("Processing Successful!")
                                                            .content("Order was processed! Check Orders Tab")
                                                            .positiveText("Close")
                                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                                @Override
                                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                                    otherservices = etotherinfo.getText().toString();
                                                                    kilocounthldr = etkilocount.getText().toString();
                                                                    if (otherservices.equals("") || otherservices.equals(" ")) {
                                                                        otherservices = "None";
                                                                    }

                                                                    mDatabase.child("Bookings").child(orderid).child("bookingstatus").setValue("orderprocessed");
                                                                    mDatabase.child("Bookings").child(orderid).child("bookingservice").setValue(msspinner.getText().toString());
                                                                    mDatabase.child("Bookings").child(orderid).child("bookingtotal").setValue(ordertotal);
                                                                    mDatabase.child("Bookings").child(orderid).child("bookingfabcon").setValue(fabconitemhldr);
                                                                    mDatabase.child("Bookings").child(orderid).child("bookingfabcontotal").setValue(fabcontotal);
                                                                    mDatabase.child("Bookings").child(orderid).child("bookingpowder").setValue(powderitemhldr);
                                                                    mDatabase.child("Bookings").child(orderid).child("bookingpowdertotal").setValue(powdertotal);
                                                                    mDatabase.child("Bookings").child(orderid).child("bookingoveralltotal").setValue(overalltotal);
                                                                    mDatabase.child("Bookings").child(orderid).child("bookingotherservices").setValue(otherservices);
                                                                    mDatabase.child("Bookings").child(orderid).child("bookingkilocount").setValue(kilocounthldr);

                                                                    displayFinalTotal(tvoveralltotal);

                                                                    SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                                                    editor.putString("booktransaction", "false");
                                                                    editor.putString("bookid", "none");
                                                                    editor.putString(orderid + "bookservice", msspinner.getText().toString());
                                                                    editor.putString(orderid + "bookpowder", powderitemhldr);
                                                                    editor.putString(orderid + "bookpowdertotal", powdertotal);
                                                                    editor.putString(orderid + "bookfabcon", fabconitemhldr);
                                                                    editor.putString(orderid + "bookfabcontotal", fabcontotal);
                                                                    editor.putString(orderid + "booktotal", ordertotal);
                                                                    editor.putString(orderid + "bookoveralltotal", overalltotal);
                                                                    editor.putString(orderid + "bookingotherservices", otherservices);
                                                                    editor.putString(orderid + "bookingkilocount", kilocounthldr);
                                                                    editor.putString(orderid + "bookstatus", "processed");
                                                                    editor.apply();

                                                                    tvorderstatus.setText("Order Processed and Picked up");
                                                                    tvorderservice.setText("The Service requested: " + msspinner.getText().toString());
                                                                    tvkilocount.setText("Service Kilo Count: " + kilocounthldr);
                                                                    tvorderpowder.setText("Washing Powder requested: " + powderspinner.getText().toString());
                                                                    tvorderfabcon.setText("Fabric Conditioner requested: " + fabconspinner.getText().toString());
                                                                    tvotherinfo.setText("Other Services/Information: "+otherservices);
                                                                    tvordertotal.setText("Approximate Final Payout: "+overalltotal);

                                                                    btnFinish.setText("Deliver");
                                                                    btnGetDirections.setText("Search Again");
                                                                    btnGetDirections.setOnClickListener(new View.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(View view) {
                                                                            Fragment fragment = new BR_MainFragment();
                                                                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                                                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                                            fragmentTransaction.remove(new BR_CurrentTransFragment());
                                                                            fragmentTransaction.replace(R.id.contentContainer, fragment);
                                                                            fragmentTransaction.commit();
                                                                        }
                                                                    });

                                                                    // <--------------------------------------------FOR DELIVERY BACK 2 CUSTOMER-------------------------------------------->
                                                                    btnFinish.setOnClickListener(new View.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(View view) {
                                                                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                                                                            LayoutInflater inflater = getActivity().getLayoutInflater();
                                                                            View dialogView = inflater.inflate(R.layout.popup_choice, null);
                                                                            dialogBuilder.setView(dialogView);

                                                                            final AlertDialog OptionalertDialog = dialogBuilder.create();
                                                                            Window window = OptionalertDialog.getWindow();
                                                                            window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                                                            window.setGravity(Gravity.CENTER);

                                                                            final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                                                                            final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                                                                            final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                                                                            Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Poppins-Bold.otf");

                                                                            optionheader.setTypeface(headerfont);
                                                                            optionheader.setText("Finish Booking?");

                                                                            negative.setOnClickListener(new View.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(View view) {
                                                                                    OptionalertDialog.dismiss();
                                                                                }
                                                                            });

                                                                            positive.setOnClickListener(new View.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(View view) {
                                                                                    finaldialog = new MaterialDialog.Builder(getActivity())
                                                                                            .title("Successful!")
                                                                                            .content("Order was successfully done")
                                                                                            .positiveText("Close")
                                                                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                                                                @Override
                                                                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                                                                    OptionalertDialog.dismiss();
                                                                                                    mDatabase.child("Bookings").child(orderid).child("bookingstatus").setValue("delivered");
                                                                                                    SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                                                                                    editor.putString("booktransaction", "false");
                                                                                                    editor.putString("bookid", "none");
                                                                                                    editor.putString(orderid + "bookname", "none");
                                                                                                    editor.putString(orderid + "bookaddress", "none");
                                                                                                    editor.putString(orderid + "bookcontact", "none");
                                                                                                    editor.putString(orderid + "booktimestamp", "none");
                                                                                                    editor.putString(orderid + "booktotal", "none");
                                                                                                    editor.putString(orderid + "booklatitude", "none");
                                                                                                    editor.putString(orderid + "booklongitude", "none");
                                                                                                    editor.putString(orderid + "bookservice", "none");
                                                                                                    editor.putString(orderid + "bookpowder", "none");
                                                                                                    editor.putString(orderid + "bookpowdertotal", "none");
                                                                                                    editor.putString(orderid + "bookfabcon", "none");
                                                                                                    editor.putString(orderid + "bookfabcontotal", "none");
                                                                                                    editor.putString(orderid + "bookoveralltotal", "none");
                                                                                                    editor.putString(orderid + "bookcharge", "none");
                                                                                                    editor.putString(orderid + "bookingotherservices", "none");
                                                                                                    editor.putString(orderid + "bookingkilocount", "none");
                                                                                                    editor.putString(orderid + "branchminkgcount", "none");
                                                                                                    editor.apply();

                                                                                                    finaldialog.dismiss();

                                                                                                    Fragment fragment = new BR_MainFragment();
                                                                                                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                                                                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                                                                    fragmentTransaction.remove(new BR_CurrentTransFragment());
                                                                                                    fragmentTransaction.replace(R.id.contentContainer, fragment);
                                                                                                    fragmentTransaction.commit();
                                                                                                }
                                                                                            })
                                                                                            .show();
                                                                                }
                                                                            });
                                                                            OptionalertDialog.show();
                                                                        }
                                                                    });
                                                                    dialog.dismiss();
                                                                }
                                                            })
                                                            .show();
                                                }
                                            });
                                            FinalOptionalertDialog.show();
                                        }
                                    }
                                });
                                CompleteOrderdialog.setCancelable(false);
                                CompleteOrderdialog.show();
                            }
                        });
                    }
                });
                OptionalertDialog.show();
            }
        });
    }


    public void RetrieveBookingData() {
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        orderid = prefs.getString("bookid", "ID");
        ordername = prefs.getString(orderid+"bookcustname", "Name");
        orderaddress = prefs.getString(orderid+"bookaddress", "Address");
        ordercontact = prefs.getString(orderid+"bookcontact", "Contact");
        ordertimestamp = prefs.getString(orderid+"booktimestamp", "Timestamp");
        orderlat = prefs.getString(orderid+"booklatitude", "Latitude");
        orderlong = prefs.getString(orderid+"booklongitude", "Longitude");
        orderstat = prefs.getString(orderid+"bookstatus", "Status");
        ordercharge = prefs.getString(orderid+"bookcharge", "Delivery Charge");
        orderbranch = prefs.getString(orderid+"branchname", "Branch Name");
        orderbranchid = prefs.getString(orderid+"branchid", "Branch ID");
        orderbranchmainid = prefs.getString(orderid+"branchmainid", "Branch Main ID");
        orderotherinfo = prefs.getString(orderid+"bookingotherservices", "Additional");
        orderkilocount = prefs.getString(orderid+"bookingkilocount", "Kilo Count");
        orderbranchminkgcount = prefs.getString(orderid+"branchminkgcount", "Min Kg Count");

        tvorderid.setText("Booking ID: " + orderid);
        tvordername.setText("Customer Name:" + ordername);
        tvorderaddress.setText("Customer Address: " + orderaddress);
        tvordercontact.setText("Customer Contact: " + ordercontact);
        tvordertimestamp.setText("Booking Timestamp: " + ordertimestamp);
        tvorderstatus.setText("Order not yet Picked up");
        tvorderservice.setText("The Service requested: Order not yet picked up");
        tvkilocount.setText("Service Kilo Count: Order not yet picked up");
        tvotherinfo.setText("Other Services/Information: Order no yet picked up");
        tvorderpowder.setText("Washing Powder requested: Order not yet picked up");
        tvorderfabcon.setText("Fabric Conditioner requested: Order not yet picked up");
        tvordertotal.setText("Final Payout: Order not yet picked up");
    }

    private Location getLastKnownLocation() {
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission Check");
                Toast.makeText(getActivity(), "Turn on GPS first", Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
                return null;
            }
            else{
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    bestLocation = l;
                    latitude  = String.valueOf(bestLocation.getLatitude());
                    longitude = String.valueOf(bestLocation.getLongitude());

                    mDatabase.child("Bookings").child(orderid).child("driverlat").setValue(latitude);
                    mDatabase.child("Bookings").child(orderid).child("driverlong").setValue(longitude);
                }
            }

        }
        return bestLocation;
    }

    public void onChangeLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        {
            LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    getLastKnownLocation();
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {}

                @Override
                public void onProviderEnabled(String s) {}

                @Override
                public void onProviderDisabled(String s) {}
            };
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                    locationListener);
        }
    }


        public void addPowder(final TextView tvpowdertotal, final TextView tvoveralltotal, final MaterialSpinner pspinner){
            inputQuan = new MaterialDialog.Builder(getActivity())
                    .title("Add Washing Powder")
                    .content("How many would you like?")
                    .inputType(InputType.TYPE_CLASS_NUMBER)
                    .input("Quantity", String.valueOf(powderquan), new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                            if(String.valueOf(inputQuan.getInputEditText().getText()).equals("")) {
                                Toast.makeText(getActivity(), "Quantity Field cannot be empty!", Toast.LENGTH_SHORT).show();
                                inputQuan.dismiss();
                                addPowder(tvpowdertotal, tvoveralltotal, pspinner);
                            }
                            if(String.valueOf(inputQuan.getInputEditText().getText()).equals("0")) {
                                Toast.makeText(getActivity(), "Quantity cannot be 0. Insert value", Toast.LENGTH_SHORT).show();
                                inputQuan.dismiss();
                                addPowder(tvpowdertotal, tvoveralltotal, pspinner);
                            }
                            else{
                                if(powderstatus.equals("false")){
                                    powderstatus = "true";}
                                else{
                                    overalltotal = String.valueOf(Float.parseFloat(overalltotal) - Float.parseFloat(powdertotalhldr));
                                }
                                pstringtoparse = pspinner.getText().toString();
                                pparser = pstringtoparse;
                                pdelims = "[/]+";
                                ptokens = pparser.split(pdelims);

                                powdertotal = String.valueOf(Float.parseFloat(inputQuan.getInputEditText().getText().toString()) * Float.parseFloat(ptokens[1]));
                                powderitemhldr = ptokens[0] + "(" + String.valueOf(Float.parseFloat(inputQuan.getInputEditText().getText().toString()))+" pcs)";
                                powderquan = Integer.parseInt(inputQuan.getInputEditText().getText().toString());
                                overalltotal = String.valueOf(Float.parseFloat(overalltotal) + Float.parseFloat(powdertotal));
                                tvpowdertotal.setText("Powder Price("+inputQuan.getInputEditText().getText().toString()+" pcs):"+ powdertotal);
                                powdertotalhldr = powdertotal;
                                displayFinalTotal(tvoveralltotal);
                            }
                        }
                    })
                    .positiveText("Continue")
                    .neutralText("Cancel")
                    .cancelable(false)
                    .show();
       }


    public void addFabcon(final TextView tvfabcontotal, final TextView tvoveralltotal, final MaterialSpinner fspinner){
            inputQuan = new MaterialDialog.Builder(getActivity())
                    .title("Add Fab Con")
                    .content("How many would you like?")
                    .inputType(InputType.TYPE_CLASS_NUMBER)
                    .input("Quantity", String.valueOf(fabconquan), new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                            if(String.valueOf(inputQuan.getInputEditText().getText()).equals("")) {
                                Toast.makeText(getActivity(), "Quantity Field cannot be empty!", Toast.LENGTH_SHORT).show();
                                inputQuan.dismiss();
                                addFabcon(tvfabcontotal, tvoveralltotal, fspinner);
                            }
                            if(String.valueOf(inputQuan.getInputEditText().getText()).equals("0")) {
                                Toast.makeText(getActivity(), "Quantity cannot be 0. Insert value", Toast.LENGTH_SHORT).show();
                                inputQuan.dismiss();
                                addFabcon(tvfabcontotal, tvoveralltotal , fspinner);
                            }
                            else{
                                if(fabconstatus.equals("false")){
                                    fabconstatus = "true";
                                }else{
                                    overalltotal = String.valueOf(Float.parseFloat(overalltotal) - Float.parseFloat(fabcontotalhldr));
                                }

                                fstringtoparse = fspinner.getText().toString();
                                fparser = fstringtoparse;
                                fdelims = "[/]+";
                                ftokens = fparser.split(fdelims);

                                fabcontotal = String.valueOf(Float.parseFloat(inputQuan.getInputEditText().getText().toString()) * Float.parseFloat(ftokens[1]));
                                fabconitemhldr = ftokens[0] + "(" + String.valueOf(Float.parseFloat(inputQuan.getInputEditText().getText().toString()))+" pcs)";
                                fabconquan = Integer.parseInt(inputQuan.getInputEditText().getText().toString());
                                overalltotal = String.valueOf(Float.parseFloat(overalltotal) + Float.parseFloat(fabcontotal));
                                tvfabcontotal.setText("Fabcon Price("+inputQuan.getInputEditText().getText().toString()+" pcs):"+ fabcontotal);
                                fabcontotalhldr = fabcontotal;
                                displayFinalTotal(tvoveralltotal);
                            }
                        }
                    })
                    .positiveText("Continue")
                    .neutralText("Cancel")
                    .cancelable(false)
                    .show();
    }

    public void displayFinalTotal(TextView tvoveralltotal){
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString str1= new SpannableString("Service Total(Per "+orderbranchminkgcount+" Kilos): ");
        str1.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str1.length(), 0);
        builder.append(str1);

        SpannableString str2= new SpannableString( "₱"+ordertotal+"\n");
        str2.setSpan(new ForegroundColorSpan(Color.BLUE), 0, str2.length(), 0);
        builder.append(str2);

        SpannableString str3= new SpannableString("Powder Price Total: ");
        str3.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str3.length(), 0);
        builder.append(str3);

        SpannableString str4= new SpannableString( "₱"+powdertotal+"\n");
        str4.setSpan(new ForegroundColorSpan(Color.BLUE), 0, str4.length(), 0);
        builder.append(str4);

        SpannableString str5= new SpannableString("FabCon Price Total: ");
        str5.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str5.length(), 0);
        builder.append(str5);

        SpannableString str6= new SpannableString( "₱"+fabcontotal+"\n");
        str6.setSpan(new ForegroundColorSpan(Color.BLUE), 0, str6.length(), 0);
        builder.append(str6);

        SpannableString str7= new SpannableString("Service Charge: ");
        str7.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str7.length(), 0);
        builder.append(str7);

        SpannableString str8= new SpannableString( "₱"+orderbranchservicefee+"\n");
        str8.setSpan(new ForegroundColorSpan(Color.BLUE), 0, str8.length(), 0);
        builder.append(str8);

        SpannableString str9= new SpannableString("Approximate Final Payout: ");
        str9.setSpan(new StyleSpan(Typeface.BOLD), 0, str9.length(), 0);
        str9.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str9.length(), 0);
        builder.append(str9);

        SpannableString str10= new SpannableString( "₱"+String.valueOf(Float.valueOf(orderbranchservicefee) + Float.valueOf(overalltotal)));
        str10.setSpan(new StyleSpan(Typeface.BOLD), 0, str10.length(), 0);
        str10.setSpan(new ForegroundColorSpan(Color.BLUE), 0, str10.length(), 0);
        builder.append(str10);
        tvoveralltotal.setText(builder, TextView.BufferType.SPANNABLE);
    }

    class RecheckBookingStatus extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            C_FirebaseClass firebaseFunctions = new C_FirebaseClass(getActivity());
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Bookings")
                    .child(orderid)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final C_BookingClass book = dataSnapshot.getValue(C_BookingClass.class);
                            bookingstatus = book.getBookingstatus();
                            if(bookingstatus.equals("pending")){
                                mDatabase.child("Bookings").child(orderid).child("bookingstatus").setValue("otw");
                            }
                            else if (bookingstatus.equals("otw")){
                                riderdialog = new MaterialDialog.Builder(getActivity())
                                        .title("Notification")
                                        .content("Booking has been accepted by another rider")
                                        .positiveText("Close")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                                editor.putString("booktransaction", "false");
                                                editor.putString("bookid", "none");
                                                editor.putString(orderid + "bookcustname", "none");
                                                editor.putString(orderid + "bookaddress", "none");
                                                editor.putString(orderid + "bookcontact", "none");
                                                editor.putString(orderid + "booktimestamp", "none");
                                                editor.putString(orderid + "booklatitude", "none");
                                                editor.putString(orderid + "booklongitude", "none");
                                                editor.putString(orderid + "bookstatus", "none");
                                                editor.putString(orderid + "bookcharge", "none");
                                                editor.putString(orderid + "branchname", "none");
                                                editor.putString(orderid + "branchid", "none");
                                                editor.putString(orderid + "branchmainid", "none");
                                                editor.putString(orderid + "bookingotherservices", "none");
                                                editor.putString(orderid + "bookingkilocount", "none");
                                                editor.putString(orderid + "branchminkgcount", "none");
                                                editor.apply();

                                                riderdialog.dismiss();

                                                Fragment fragment = new BR_MainFragment();
                                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                fragmentTransaction.remove(new BR_CurrentTransFragment());
                                                fragmentTransaction.replace(R.id.contentContainer, fragment);
                                                fragmentTransaction.commit();
                                            }
                                        })
                                        .show();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
            return null;
        }
    }

    class RetrieveBranchServices extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            C_FirebaseClass firebaseFunctions = new C_FirebaseClass(getActivity());
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Branch")
                    .child(rbranchid)
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final C_BranchClass branch = dataSnapshot.getValue(C_BranchClass.class);
                            minkgcount = branch.getMinWeight();
                            orderbranchservicefee = branch.getServiceFee();
                            serviceshldr = branch.getService();
                            fabconhldr = branch.getFabcon();
                            powderhldr = branch.getPowder();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
            return null;
        }
    }

    public void InitializeSpinners(){
        serviceshldr.add("Select Service");
        powderhldr.add("Select Washing Powder");
        fabconhldr.add("Select Fabric Conditioner");
        powderhldr.add("User will provide Powder");
        fabconhldr.add("User will provide Fabcon");
    }

    public void RetrieveBranchID(){
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        rbranchid = prefs.getString("riderbranchid", "BranchID");
    }
}
