package androidstudio.assistmelaundry;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by JP on 12/1/2017.
 */

public class FA_MainClientActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    //Variables
    String ordertransaction,name,email,address,contact, rbranchid;
    android.support.v4.app.FragmentTransaction transaction;

    //Toolbar
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbartitle;

    //Preference
    private String USER_TYPE;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activityclient_main);
        ButterKnife.bind(this);
        RetrieveUserData();
        InitializeDesign();
        InitializeFunctions();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitleTextColor(Color.WHITE);
    }

    public void InitializeDesign(){
        Typeface headerfont = Typeface.createFromAsset(getAssets(),  "fonts/Poppins-Bold.otf");
        toolbartitle.setTypeface(headerfont);
    }

    public void InitializeFunctions(){
        BottomBar bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if(tabId == R.id.tab_orders){
                    RetrieveUserData();
                    if(ordertransaction.equals("false")){
                        toolbartitle.setText("Laundrie");
                        Fragment fragmentsreport = new BR_MainFragment();
                        transaction = getSupportFragmentManager().beginTransaction();
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                        transaction.replace(R.id.contentContainer, fragmentsreport);
                        transaction.commit();
                    }
                    else if(ordertransaction.equals("true")){
                        toolbartitle.setText("Laundrie (On the way)");
                        Fragment fragmentsreport = new BR_CurrentTransFragment();
                        transaction = getSupportFragmentManager().beginTransaction();
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                        transaction.replace(R.id.contentContainer, fragmentsreport);
                        transaction.commit();
                    }
                    else if(ordertransaction.equals("deliverytrue")){
                        toolbartitle.setText("Laundrie (On the way)");
                        Fragment fragmentsreport = new BR_CurrentDeliveryFragment();
                        transaction = getSupportFragmentManager().beginTransaction();
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                        transaction.replace(R.id.contentContainer, fragmentsreport);
                        transaction.commit();
                    }
                }
                if (tabId == R.id.tab_currentorders) {
                    Fragment fragmentsmap = new BR_CurrentOrdersFragment();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                    transaction.replace(R.id.contentContainer, fragmentsmap);
                    transaction.commit();
                }
                if (tabId == R.id.tab_history) {
                    Fragment fragmentsmap = new BR_BookHistoryFragment();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                    transaction.replace(R.id.contentContainer, fragmentsmap);
                    transaction.commit();
                }
                if(tabId == R.id.tab_profile){
                    Fragment fragmentsreport = new FA_ProfileFragment();
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                    transaction.replace(R.id.contentContainer, fragmentsreport);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            final C_FirebaseClass FAFirebaseClass = new C_FirebaseClass(FA_MainClientActivity.this);
            FAFirebaseClass.getUserInstance().signOut();
            SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
            editor.remove("status");
            editor.remove("timehldr");
            editor.remove("username");
            editor.remove("useremail");
            editor.remove("useraddress");
            editor.remove("usercontact");
            editor.remove("riderbranchid");
            editor.apply();
            Intent intent = new Intent(FA_MainClientActivity.this, FA_LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void RetrieveUserData(){
        SharedPreferences prefs = getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        ordertransaction = prefs.getString("booktransaction", "order");
        name = prefs.getString("username", "Name");
        email = prefs.getString("useremail", "Email");
        address = prefs.getString("useraddress", "Address");
        contact = prefs.getString("usercontact", "Contact");
        rbranchid = prefs.getString("riderbranchid", "BranchID");
    }

    public void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
//                            Toast.makeText(FA_MainClientActivity.this, "Success", Toast.LENGTH_LONG).show();
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                            Toast.makeText(FA_MainClientActivity.this, "GPS is not on", Toast.LENGTH_LONG).show();
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(FA_MainClientActivity.this, 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Toast.makeText(FA_MainClientActivity.this, "Settings change not allowed", Toast.LENGTH_SHORT).show();
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1000) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(FA_MainClientActivity.this, "Suspended", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(FA_MainClientActivity.this, "Failed", Toast.LENGTH_SHORT).show();
    }
}
