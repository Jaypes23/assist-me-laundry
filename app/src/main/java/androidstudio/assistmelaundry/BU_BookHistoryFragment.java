package androidstudio.assistmelaundry;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

/**
 * Created by JP on 12/4/2017.
 */

public class BU_BookHistoryFragment extends android.support.v4.app.Fragment {

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    //Variables
    public String uid,orderid,orderdate,address,contact,orderservice, total, orderstatus;
    public String orderpowder, orderpowdertotal, orderfabcon, orderfabcontotal, orderoveralltotal,ordercharge, orderkilocount, orderotherinfo;
    public String orderbranch, orderbranchaddress, orderbranchcontact, orderbranchminkgcount;
    C_BookingClass lm;
    ArrayList<C_BookingClass> listmenu;
    ArrayList<String> bookdetails;
    FA_HistoryAdapter itemAdapter;

    //UI
    @BindView(R.id.historylist)
    ListView orderlist;
    @BindView(R.id.header)
    TextView header;

    MaterialDialog dialog;

    public BU_BookHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookhistory, container, false);
        ButterKnife.bind(this, view);
        InitializeDesign();
        InitializeFunctions();
        return view;
    }

    public void InitializeDesign(){
        Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Bold.otf");
        header.setTypeface(headerfont);
    }

    public void InitializeFunctions(){
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        listmenu = new ArrayList<>();
        bookdetails = new ArrayList<>();

        RetrieveBookingHistory();

        itemAdapter = new FA_HistoryAdapter(getActivity(), listmenu);
        orderlist.setAdapter(itemAdapter);

        orderlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.popup_bookinginfo, null);
                dialogBuilder.setView(dialogView);
                String valhldr = bookdetails.get(position);
                String stringtoparse = valhldr;
                String parser = stringtoparse;
                String delims = "[<>]+";
                String[] tokens = parser.split(delims);
                String ordid = String.valueOf(tokens[0]);
                String orddate = String.valueOf(tokens[1]);
                String ordadd = String.valueOf(tokens[2]);
                String ordcon = String.valueOf(tokens[3]);
                String ordtotal = String.valueOf(tokens[4]);
                String ordservice = String.valueOf(tokens[5]);
                String ordstatus = String.valueOf(tokens[6]);
                String orderpowder = String.valueOf(tokens[7]);
                String orderpowdertotal = String.valueOf(tokens[8]);
                String orderfabcon = String.valueOf(tokens[9]);
                String orderfabcontotal = String.valueOf(tokens[10]);
                String orderoverall = String.valueOf(tokens[11]);
                String orderdelcharge = String.valueOf(tokens[12]);
                String orderkilocount = String.valueOf(tokens[13]);
                String orderotherinfo = String.valueOf(tokens[14]);
                String orderbranch = String.valueOf(tokens[15]);
                String orderbranchaddress = String.valueOf(tokens[16]);
                String orderbranchcontact = String.valueOf(tokens[17]);
                String orderbranchminkgcount = String.valueOf(tokens[18]);

                final TextView tvordheader = (TextView) dialogView.findViewById(R.id.header);
                final TextView tvordbranch = (TextView) dialogView.findViewById(R.id.delbranch);
                final TextView tvordbranchhldr = (TextView) dialogView.findViewById(R.id.delbranchhldr);
                final TextView tvordbranchaddress = (TextView) dialogView.findViewById(R.id.delbranchaddress);
                final TextView tvordbranchaddresshldr = (TextView) dialogView.findViewById(R.id.delbranchaddresshldr);
                final TextView tvordbranchcontact = (TextView) dialogView.findViewById(R.id.delbranchcontact);
                final TextView tvordbranchcontacthldr = (TextView) dialogView.findViewById(R.id.delbranchcontacthldr);
                final TextView tvordernum = (TextView) dialogView.findViewById(R.id.delordernum);
                final TextView tvordtotal = (TextView) dialogView.findViewById(R.id.deltotal);
                final TextView tvordstatus = (TextView) dialogView.findViewById(R.id.delstatus);
                final TextView tvordaddress = (TextView) dialogView.findViewById(R.id.deladdress);
                final TextView tvordaddresshldr = (TextView) dialogView.findViewById(R.id.deladdresshldr);
                final TextView tvordcontact = (TextView) dialogView.findViewById(R.id.delcontact);
                final TextView tvordcontacthldr = (TextView) dialogView.findViewById(R.id.delcontacthldr);
                final TextView tvordchange = (TextView) dialogView.findViewById(R.id.delchangefor);
                final TextView tvordchangehldr = (TextView) dialogView.findViewById(R.id.delchangeforhldr);
                final TextView tvordtimestamp = (TextView) dialogView.findViewById(R.id.deltimestamp);
                final TextView tvordtimestamphldr = (TextView) dialogView.findViewById(R.id.deltimestamphldr);
                final TextView tvordpowder = (TextView) dialogView.findViewById(R.id.delpowder);
                final TextView tvordpowderhldr = (TextView) dialogView.findViewById(R.id.delpowderhldr);
                final TextView tvordfabcon = (TextView) dialogView.findViewById(R.id.delfabcon);
                final TextView tvordfabconhldr = (TextView) dialogView.findViewById(R.id.delfabconhldr);
                final TextView tvordotherinfo = (TextView) dialogView.findViewById(R.id.delotherinfo);
                final TextView tvordotherinfohldr = (TextView) dialogView.findViewById(R.id.delotherinfohldr);
                final TextView tvordkilocount = (TextView) dialogView.findViewById(R.id.delservicekilo);
                final TextView tvordkilocounthldr = (TextView) dialogView.findViewById(R.id.delservicekilohldr);

                Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Bold.otf");
                Typeface subheaderfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Medium.otf");
                Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Regular.otf");

                tvordheader.setTypeface(headerfont);
                tvordbranch.setTypeface(subheaderfont);
                tvordbranchhldr.setTypeface(regulartextfont);
                tvordbranchaddress.setTypeface(subheaderfont);
                tvordbranchaddresshldr.setTypeface(regulartextfont);
                tvordbranchcontact.setTypeface(subheaderfont);
                tvordbranchcontacthldr.setTypeface(regulartextfont);
                tvordstatus.setTypeface(subheaderfont);
                tvordernum.setTypeface(subheaderfont);
                tvordtotal.setTypeface(subheaderfont);
                tvordaddress.setTypeface(subheaderfont);
                tvordaddresshldr.setTypeface(regulartextfont);
                tvordcontact.setTypeface(subheaderfont);
                tvordcontacthldr.setTypeface(regulartextfont);
                tvordchange.setTypeface(subheaderfont);
                tvordchangehldr.setTypeface(regulartextfont);
                tvordtimestamp.setTypeface(subheaderfont);
                tvordtimestamphldr.setTypeface(regulartextfont);
                tvordpowder.setTypeface(subheaderfont);
                tvordpowderhldr.setTypeface(regulartextfont);
                tvordfabcon.setTypeface(subheaderfont);
                tvordfabconhldr.setTypeface(regulartextfont);
                tvordotherinfo.setTypeface(subheaderfont);
                tvordotherinfohldr.setTypeface(regulartextfont);
                tvordkilocount.setTypeface(subheaderfont);
                tvordkilocounthldr.setTypeface(regulartextfont);

                if(ordstatus.equals("otw")){
                    ordstatus = "Rider on the way";
                    tvordtotal.setText("Order is not yet processed");
                }else if(ordstatus.equals("riderarrived")){
                    ordstatus = "Rider arrived";
                    tvordtotal.setText("Order is not yet processed");
                }
                else if(ordstatus.equals("orderprocessed")){
                    ordstatus = "Order has been processed";
                    displayFinalTotal(orderbranchminkgcount,ordtotal,orderpowdertotal,orderfabcontotal,orderoverall,orderdelcharge,tvordtotal);
                }
                else if(ordstatus.equals("delivery")){
                    ordstatus = "Rider is on the way back";
                    displayFinalTotal(orderbranchminkgcount,ordtotal,orderpowdertotal,orderfabcontotal,orderoverall,orderdelcharge,tvordtotal);
                }
                else if(ordstatus.equals("deliverarrived")){
                    ordstatus = "Rider has arrived with your Laundry";
                    displayFinalTotal(orderbranchminkgcount,ordtotal,orderpowdertotal,orderfabcontotal,orderoverall,orderdelcharge,tvordtotal);
                }
                else if(ordstatus.equals("delivered")){
                    ordstatus = "Delivery successful! Thank you for using Laundrie";
                    displayFinalTotal(orderbranchminkgcount,ordtotal,orderpowdertotal,orderfabcontotal,orderoverall,orderdelcharge,tvordtotal);
                }

                tvordernum.setText(ordid);
                tvordbranchhldr.setText(orderbranch);
                tvordbranchaddresshldr.setText(orderbranchaddress);
                tvordbranchcontacthldr.setText(orderbranchcontact);
                tvordstatus.setText(ordstatus);
                tvordaddresshldr.setText(ordadd);
                tvordcontacthldr.setText(ordcon);
                tvordchangehldr.setText(ordservice);
                tvordpowderhldr.setText(orderpowder);
                tvordfabconhldr.setText(orderfabcon);
                tvordtimestamphldr.setText(orddate);
                tvordkilocounthldr.setText(orderkilocount);
                tvordotherinfohldr.setText(orderotherinfo);

                final AlertDialog alertDialog = dialogBuilder.create();
                Window window = alertDialog.getWindow();
                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);

                // Cancel Button
                FancyButton cancel_btn = (FancyButton) dialogView.findViewById(R.id.buttoncancellist);
                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.hide();
                    }
                });

                alertDialog.show();
            }});
    }

    public void RetrieveBookingHistory() {
        C_FirebaseClass firebaseFunctions = new C_FirebaseClass(getActivity());
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        mRootRef.child("Bookings")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        final C_BookingClass book = dataSnapshot.getValue(C_BookingClass.class);
                        uid = book.getUid();
                        orderid = book.getBookingid();
                        orderdate = book.getBookingtimestamp();
                        address = book.getBookingaddress();
                        contact = book.getBookingcontact();
                        total = book.getBookingtotal();
                        orderservice = book.getBookingservice();
                        orderstatus = book.getBookingstatus();
                        orderpowder = book.getBookingpowder();
                        orderpowdertotal = book.getBookingpowdertotal();
                        orderfabcon = book.getBookingfabcon();
                        orderfabcontotal = book.getBookingfabcontotal();
                        orderoveralltotal = book.getBookingoveralltotal();
                        ordercharge = book.getBookingcharge();
                        orderkilocount = book.getBookingkilocount();
                        orderotherinfo = book.getBookingotherservices();
                        orderbranch = book.getBranchname();
                        orderbranchaddress = book.getBranchaddress();
                        orderbranchcontact = book.getBranchcontact();
                        orderbranchminkgcount= book.getBranchminkgcount();

                        if(uid !=null && orderid !=null && address !=null && contact !=null && total !=null && orderdate !=null
                                && orderservice !=null && orderstatus !=null && orderpowder !=null && orderpowdertotal !=null && orderfabcon != null
                                && orderfabcontotal !=null && orderoveralltotal !=null && ordercharge != null && orderkilocount != null
                                && orderotherinfo != null && orderbranch != null && orderbranchaddress != null && orderbranchcontact != null && orderbranchminkgcount != null){
                        if(mAuth.getCurrentUser().getUid().equals(uid)){
                            lm = new C_BookingClass();
                            lm.setBookingid(orderid);
                            lm.setBookingtimestamp(orderdate);
                            lm.setBookingaddress(address);
                            lm.setBookingcontact(contact);
                            lm.setBookingtotal(total);
                            lm.setBookingservice(orderservice);
                            lm.setBookingstatus(orderstatus);
                            lm.setBookingpowder(orderpowder);
                            lm.setBookingpowdertotal(orderpowdertotal);
                            lm.setBookingfabcon(orderfabcon);
                            lm.setBookingfabcontotal(orderfabcontotal);
                            lm.setBookingoveralltotal(orderoveralltotal);
                            lm.setBookingcharge(ordercharge);
                            lm.setBookingkilocount(orderkilocount);
                            lm.setBookingotherservices(orderotherinfo);
                            lm.setBranchname(orderbranch);
                            lm.setBranchaddress(orderbranchaddress);
                            lm.setBranchcontact(orderbranchcontact);
                            lm.setBranchminkgcount(orderbranchminkgcount);
                            listmenu.add(lm);
                            itemAdapter.notifyDataSetChanged();
                            bookdetails.add(orderid+"<>"+orderdate+"<>"+address+"<>"+contact+"<>"+total+"<>"+orderservice+"<>"
                                    +orderstatus+"<>"+orderpowder +"<>"+orderpowdertotal+"<>"+orderfabcon+"<>"+orderfabcontotal+"<>"
                                    +orderoveralltotal+"<>"+ordercharge+"<>"+orderkilocount +"<>"+orderotherinfo+"<>"+orderbranch+"<>"
                                    +orderbranchaddress+"<>"+orderbranchcontact +"<>"+orderbranchminkgcount);
                        }
                    }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public String displayFinalTotal(String minkgcount, String finaltotal, String finalpowdertotal, String finalfabcontotal, String finaloveralltotal, String finalcharge, TextView displaytotal){
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString str1= new SpannableString("Service Total(Per "+minkgcount+" Kilos): ");
        str1.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorPrimary)), 0, str1.length(), 0);
        builder.append(str1);

        SpannableString str2= new SpannableString( "₱"+finaltotal+"\n");
        str2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorPrimary)), 0, str2.length(), 0);
        builder.append(str2);

        SpannableString str3= new SpannableString("Powder Price Total: ");
        str3.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.textColor)), 0, str3.length(), 0);
        builder.append(str3);

        SpannableString str4= new SpannableString( "₱"+finalpowdertotal+"\n");
        str4.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorPrimary)), 0, str4.length(), 0);
        builder.append(str4);

        SpannableString str5= new SpannableString("FabCon Price Total: ");
        str5.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.textColor)), 0, str5.length(), 0);
        builder.append(str5);

        SpannableString str6= new SpannableString( "₱"+finalfabcontotal+"\n");
        str6.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorPrimary)), 0, str6.length(), 0);
        builder.append(str6);

        SpannableString str7= new SpannableString("Service Charge: ");
        str7.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.textColor)), 0, str7.length(), 0);
        builder.append(str7);

        SpannableString str8= new SpannableString( "₱"+finalcharge+"\n");
        str8.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorPrimary)), 0, str8.length(), 0);
        builder.append(str8);

        SpannableString str9= new SpannableString("Final Payout: ");
        str9.setSpan(new StyleSpan(Typeface.BOLD), 0, str9.length(), 0);
        str9.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.textColor)), 0, str9.length(), 0);
        builder.append(str9);

        SpannableString str10= new SpannableString( "₱"+String.valueOf(Double.valueOf(finaloveralltotal) + Double.valueOf(finalcharge)));
        str10.setSpan(new StyleSpan(Typeface.BOLD), 0, str10.length(), 0);
        str10.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorPrimary)), 0, str10.length(), 0);
        builder.append(str10);
        displaytotal.setText(builder, TextView.BufferType.SPANNABLE);

        return "Final Total";
    }

}
