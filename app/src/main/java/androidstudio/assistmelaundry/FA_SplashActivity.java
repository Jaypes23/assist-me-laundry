package androidstudio.assistmelaundry;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import static android.view.animation.AnimationUtils.loadAnimation;

/**
 * Created by JP on 2/22/2018.
 */

public class FA_SplashActivity extends Activity {
    ImageView LaundrieHeader;
    android.view.animation.Animation animFadeInHeader, animFadeInSubHeader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        LaundrieHeader = (ImageView) findViewById(R.id.headerlogo);

        animFadeInHeader = loadAnimation(getApplicationContext(), R.anim.fadein_header);
        LaundrieHeader.startAnimation(animFadeInHeader);

        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(6000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    Intent intent = new Intent(FA_SplashActivity.this,FA_LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };
        timerThread.start();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }

}

