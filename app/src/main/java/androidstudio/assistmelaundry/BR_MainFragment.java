package androidstudio.assistmelaundry;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by JP on 12/6/2017.
 */

public class BR_MainFragment extends android.support.v4.app.Fragment
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.header)
    TextView tvheader;
    @BindView(R.id.pulsator)
    PulsatorLayout pulsator;
    @BindView(R.id.btnMatching)
    FancyButton fbsearch;

    String bookid, bookaddress, bookcontact, bookcustname, bookstatus, booklat, booklong, bookttimestamp, bookkilocount, bookotherinfo;
    String bookcharge;
    String branchname, branchid, branchmainid, branchminkgcount;
    String validationforrepeation = "false";
    String USER_TYPE;
    String drivername, drivercontact, driverbranchid;
    boolean GPSStatus;
    LocationManager locationManager;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookrider, container, false);
        ButterKnife.bind(this, view);
        InitializeDesign();
        InitializeFunctions();
        return view;
    }

    public void InitializeDesign(){
        Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Bold.otf");
        tvheader.setTypeface(headerfont);
    }

    public void InitializeFunctions(){
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        RetrieveDriverData();
        fbsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckGpsStatus();
                if(GPSStatus == true){
                    pulsator.start();
                    tvheader.setText("Searching...");
                    fbsearch.setText("Cancel");
                    FindCustomer();
                }else{
                    Toast.makeText(getActivity(), "GPS must be turned on!", Toast.LENGTH_SHORT).show();
                    EnableGPSAutoMatically();
                }
            }
        });
    }

    public void FindCustomer(){
        C_FirebaseClass firebaseFunctions = new C_FirebaseClass(getActivity());
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        mRootRef.child("Bookings")
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        if (validationforrepeation.equals("false")) {
                            final C_BookingClass book = dataSnapshot.getValue(C_BookingClass.class);
                            bookcustname = book.getBookingcustname();
                            bookid = book.getBookingid();
                            bookaddress = book.getBookingaddress();
                            bookcontact = book.getBookingcontact();
                            bookstatus = book.getBookingstatus();
                            bookttimestamp = book.getBookingtimestamp();
                            booklat = book.getBookinglat();
                            booklong = book.getBookinglong();
                            bookcharge = book.getBookingcharge();
                            branchname = book.getBranchname();
                            branchid = book.getBranchid();
                            branchmainid = book.getMainbranchid();
                            bookkilocount = book.getBookingkilocount();
                            bookotherinfo = book.getBookingotherservices();
                            branchminkgcount = book.getBranchminkgcount();

                            if(bookcustname !=null && bookid !=null && bookaddress !=null && bookcontact !=null && bookstatus !=null
                                    && bookttimestamp !=null && booklat != null && booklong !=null && bookcharge !=null && branchname !=null
                                    && branchid !=null && branchmainid !=null && bookkilocount !=null && bookotherinfo !=null && branchminkgcount !=null){

                            if(driverbranchid.equals(branchid)) {
                                if (bookstatus.equals("pending")) {
                                    validationforrepeation = "true";
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                                    LayoutInflater inflater = getActivity().getLayoutInflater();
                                    View dialogView = inflater.inflate(R.layout.popup_bookfoundrider, null);
                                    dialogBuilder.setView(dialogView);

                                    final AlertDialog OptionalertDialog = dialogBuilder.create();
                                    Window window = OptionalertDialog.getWindow();
                                    window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    window.setGravity(Gravity.CENTER);

                                    final TextView tvheader = (TextView) dialogView.findViewById(R.id.header);
                                    final TextView tvbookid = (TextView) dialogView.findViewById(R.id.delordernum);
                                    final TextView tvstatus = (TextView) dialogView.findViewById(R.id.delstatus);
                                    final TextView tvname = (TextView) dialogView.findViewById(R.id.delcustname);
                                    final TextView tvaddress = (TextView) dialogView.findViewById(R.id.deladdress);
                                    final TextView tvcontact = (TextView) dialogView.findViewById(R.id.delcontact);
                                    final TextView tvtimestamp = (TextView) dialogView.findViewById(R.id.deltimestamp);
                                    final FancyButton btncancel = (FancyButton) dialogView.findViewById(R.id.buttonclose);
                                    final FancyButton btnaccept = (FancyButton) dialogView.findViewById(R.id.buttonaccept);

                                    Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Poppins-Bold.otf");
                                    Typeface subheaderfont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Poppins-Medium.otf");
                                    Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Poppins-Regular.otf");

                                    tvheader.setTypeface(headerfont);
                                    tvstatus.setTypeface(subheaderfont);
                                    tvname.setTypeface(regulartextfont);
                                    tvaddress.setTypeface(regulartextfont);
                                    tvcontact.setTypeface(regulartextfont);
                                    tvtimestamp.setTypeface(regulartextfont);

                                    tvbookid.setText(bookid);
                                    tvstatus.setText("Status: Pending");
                                    tvname.setText("Customer Name: " + bookcustname);
                                    tvaddress.setText("Address: " + bookaddress);
                                    tvcontact.setText("Contact #: " + bookcontact);
                                    tvtimestamp.setText("Booking Timestamp: " + bookttimestamp);

                                    btncancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            OptionalertDialog.dismiss();
                                            validationforrepeation = "false";
                                            tvheader.setText("Welcome");
                                            fbsearch.setText("Find Customer");
                                            pulsator.stop();
                                            fbsearch.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    CheckGpsStatus();
                                                    if (GPSStatus == true) {
                                                        pulsator.start();
                                                        tvheader.setText("Searching...");
                                                        fbsearch.setText("Cancel");
                                                        FindCustomer();
                                                    } else {
                                                        displayPromptForEnablingGPS(getActivity());
                                                    }
                                                }
                                            });
                                        }
                                    });

                                    btnaccept.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            OptionalertDialog.dismiss();
                                            SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                            editor.putString("booktransaction", "true");
                                            editor.putString("bookid", bookid);
                                            editor.putString(bookid + "bookcustname", bookcustname);
                                            editor.putString(bookid + "bookcontact", bookcontact);
                                            editor.putString(bookid + "bookaddress", bookaddress);
                                            editor.putString(bookid + "booktimestamp", bookttimestamp);
                                            editor.putString(bookid + "booklatitude", booklat);
                                            editor.putString(bookid + "booklongitude", booklong);
                                            editor.putString(bookid + "bookcharge", bookcharge);
                                            editor.putString(bookid + "branchname", branchname);
                                            editor.putString(bookid + "branchid", branchid);
                                            editor.putString(bookid + "branchmainid", branchmainid);
                                            editor.putString(bookid + "bookingotherservices", bookkilocount);
                                            editor.putString(bookid + "bookingkilocount", bookotherinfo);
                                            editor.putString(bookid + "branchminkgcount", branchminkgcount);
                                            editor.putString(bookid + "bookstatus", "matched");
                                            editor.apply();

                                            mDatabase.child("Bookings").child(bookid).child("riderid").setValue(mAuth.getCurrentUser().getUid());
                                            mDatabase.child("Bookings").child(bookid).child("ridername").setValue(drivername);
                                            mDatabase.child("Bookings").child(bookid).child("ridercontact").setValue(drivercontact);

                                            Fragment fragment = new BR_CurrentTransFragment();
                                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                            fragmentTransaction.replace(R.id.contentContainer, fragment);
                                            fragmentTransaction.remove(new BR_MainFragment());
                                            fragmentTransaction.commit();
                                        }
                                    });
                                    OptionalertDialog.show();
                                }
                            }
                            }
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        fbsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validationforrepeation = "false";
                tvheader.setText("Welcome");
                fbsearch.setText("Find Customer");
                pulsator.stop();
                fbsearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CheckGpsStatus();
                        if(GPSStatus == true){
                            pulsator.start();
                            tvheader.setText("Searching...");
                            fbsearch.setText("Cancel");
                            FindCustomer();
                        }else{
                            displayPromptForEnablingGPS(getActivity());
                        }
                    }
                });
            }
        });
    }

    public void RetrieveDriverData(){
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        drivername = prefs.getString("username", "Name");
        drivercontact = prefs.getString("usercontact", "Contact");
        driverbranchid = prefs.getString("riderbranchid", "BranchID");
    }

    public void CheckGpsStatus(){
        locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        GPSStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
    public static void displayPromptForEnablingGPS(final Activity activity) {
        final AlertDialog.Builder builder =  new AlertDialog.Builder(activity);
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        final String message = "GPS/Location Setting must be enabled to continue";
        Toast.makeText(activity, "Location Settings is not On!", Toast.LENGTH_SHORT).show();
        builder.setMessage(message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                activity.startActivity(new Intent(action));
                                d.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });
        builder.create().show();
    }

    public void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
//                            Toast.makeText(getActivity(), "GPS Enabled", Toast.LENGTH_LONG).show();
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                            Toast.makeText(getActivity(), "GPS is not on", Toast.LENGTH_LONG).show();
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(getActivity(), 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Toast.makeText(getActivity(), "Settings change not allowed", Toast.LENGTH_SHORT).show();
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1000) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(getActivity(), "Suspended", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
    }
}