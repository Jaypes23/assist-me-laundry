package androidstudio.assistmelaundry;

/**
 * Created by JP on 12/4/2017.
 */

public class C_BookingClass {
    public String uid;
    public String bookingid;
    public String bookingcustname;
    public String bookingaddress;
    public String bookingcontact;
    public String bookingservice;
    public String bookingstatus;
    public String bookinglat;
    public String bookinglong;
    public String bookingtimestamp;
    public String bookingtotal;
    public String bookingpowder;
    public String bookingpowdertotal;
    public String bookingfabcon;
    public String bookingfabcontotal;
    public String bookingoveralltotal;
    public String bookingcharge;
    public String bookingotherservices;
    public String bookingkilocount;

    public String branchname;
    public String branchaddress;
    public String branchcontact;
    public String branchminkgcount;
    public String branchid;
    public String mainbranchid;

    public String riderid;
    public String ridername;
    public String ridercontact;
    public String riderlat;
    public String riderlong;

    public C_BookingClass(){

    }

    public C_BookingClass(String uid, String bookingid, String bookingcustname, String bookingaddress, String bookingcontact,
                          String bookingservice, String bookingstatus, String bookingtimestamp,
                          String bookinglat, String bookinglong, String bookingtotal, String riderid, String ridername, String ridercontact,
                          String bookingpowder, String bookingpowdertotal, String bookingfabcon, String bookingfabcontotal,
                          String bookingoveralltotal, String bookingcharge, String branchname, String branchaddress, String branchcontact,
                          String branchminkgcount, String branchid, String mainbranchid, String bookingotherservices, String bookingkilocount) {
        this.uid = uid;
        this.bookingid = bookingid;
        this.bookingcustname = bookingcustname;
        this.bookingaddress = bookingaddress;
        this.bookingcontact = bookingcontact;
        this.bookingservice = bookingservice;
        this.bookingstatus = bookingstatus;
        this.bookingtimestamp = bookingtimestamp;
        this.bookinglat = bookinglat;
        this.bookinglong = bookinglong;
        this.bookingtotal = bookingtotal;
        this.riderid = riderid;
        this.ridername = ridername;
        this.ridercontact = ridercontact;
        this.bookingpowder = bookingpowder;
        this.bookingpowdertotal = bookingpowdertotal;
        this.bookingfabcon = bookingfabcon;
        this.bookingfabcontotal = bookingfabcontotal;
        this.bookingoveralltotal = bookingoveralltotal;
        this.bookingcharge = bookingcharge;
        this.branchname = branchname;
        this.branchaddress = branchaddress;
        this.branchcontact = branchcontact;
        this.branchminkgcount = branchminkgcount;
        this.branchid = branchid;
        this.mainbranchid = mainbranchid;
        this.bookingotherservices = bookingotherservices;
        this.bookingkilocount = bookingkilocount;
    }

    public String getBookingid() {
        return bookingid;
    }

    public void setBookingid(String bookingid) {
        this.bookingid = bookingid;
    }

    public String getBookingcustname() {
        return bookingcustname;
    }

    public void setBookingcustname(String bookingcustname) {
        this.bookingcustname = bookingcustname;
    }

    public String getBookingaddress() {
        return bookingaddress;
    }

    public void setBookingaddress(String bookingaddress) {
        this.bookingaddress = bookingaddress;
    }

    public String getBookingcontact() {
        return bookingcontact;
    }

    public void setBookingcontact(String bookingcontact) {
        this.bookingcontact = bookingcontact;
    }

    public String getBookingservice() {
        return bookingservice;
    }

    public void setBookingservice(String bookingservice) {
        this.bookingservice = bookingservice;
    }

    public String getBookingstatus() {
        return bookingstatus;
    }

    public void setBookingstatus(String bookingstatus) {
        this.bookingstatus = bookingstatus;
    }

    public String getRidername() {
        return ridername;
    }

    public void setRidername(String ridername) {
        this.ridername = ridername;
    }

    public String getRiderlat() {
        return riderlat;
    }

    public void setRiderlat(String riderlat) {
        this.riderlat = riderlat;
    }

    public String getRiderlong() {
        return riderlong;
    }

    public void setRiderlong(String riderlong) {
        this.riderlong = riderlong;
    }

    public String getBookingtimestamp() {
        return bookingtimestamp;
    }

    public void setBookingtimestamp(String bookingtimestamp) {
        this.bookingtimestamp = bookingtimestamp;
    }

    public String getBookinglat() {
        return bookinglat;
    }

    public void setBookinglat(String bookinglat) {
        this.bookinglat = bookinglat;
    }

    public String getBookinglong() {
        return bookinglong;
    }

    public void setBookinglong(String bookinglong) {
        this.bookinglong = bookinglong;
    }

    public String getBookingtotal() {
        return bookingtotal;
    }

    public void setBookingtotal(String bookingtotal) {
        this.bookingtotal = bookingtotal;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getRiderid() {
        return riderid;
    }

    public void setRiderid(String riderid) {
        this.riderid = riderid;
    }

    public String getRidercontact() {
        return ridercontact;
    }

    public void setRidercontact(String ridercontact) {
        this.ridercontact = ridercontact;
    }

    public String getBookingpowder() {
        return bookingpowder;
    }

    public void setBookingpowder(String bookingpowder) {
        this.bookingpowder = bookingpowder;
    }

    public String getBookingpowdertotal() {
        return bookingpowdertotal;
    }

    public void setBookingpowdertotal(String bookingpowdertotal) {
        this.bookingpowdertotal = bookingpowdertotal;
    }

    public String getBookingfabcon() {
        return bookingfabcon;
    }

    public void setBookingfabcon(String bookingfabcon) {
        this.bookingfabcon = bookingfabcon;
    }

    public String getBookingfabcontotal() {
        return bookingfabcontotal;
    }

    public void setBookingfabcontotal(String bookingfabcontotal) {
        this.bookingfabcontotal = bookingfabcontotal;
    }

    public String getBookingoveralltotal() {
        return bookingoveralltotal;
    }

    public void setBookingoveralltotal(String bookingoveralltotal) {
        this.bookingoveralltotal = bookingoveralltotal;
    }

    public String getBookingcharge() {
        return bookingcharge;
    }

    public void setBookingcharge(String bookingcharge) {
        this.bookingcharge = bookingcharge;
    }

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

    public String getBranchid() {
        return branchid;
    }

    public void setBranchid(String branchid) {
        this.branchid = branchid;
    }

    public String getMainbranchid() {
        return mainbranchid;
    }

    public void setMainbranchid(String mainbranchid) {
        this.mainbranchid = mainbranchid;
    }

    public String getBookingotherservices() {
        return bookingotherservices;
    }

    public void setBookingotherservices(String bookingotherservices) {
        this.bookingotherservices = bookingotherservices;
    }

    public String getBookingkilocount() {
        return bookingkilocount;
    }

    public void setBookingkilocount(String bookingkilocount) {
        this.bookingkilocount = bookingkilocount;
    }

    public String getBranchaddress() {
        return branchaddress;
    }

    public void setBranchaddress(String branchaddress) {
        this.branchaddress = branchaddress;
    }

    public String getBranchcontact() {
        return branchcontact;
    }

    public void setBranchcontact(String branchcontact) {
        this.branchcontact = branchcontact;
    }

    public String getBranchminkgcount() {
        return branchminkgcount;
    }

    public void setBranchminkgcount(String branchminkgcount) {
        this.branchminkgcount = branchminkgcount;
    }
}
