package androidstudio.assistmelaundry;

/**
 * Created by JP on 3/21/2018.
 */

public class C_ServicesClass {
    private String Price;
    private String Service;

    public C_ServicesClass(){}

    public C_ServicesClass(String price, String service) {
        Price = price;
        Service = service;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getService() {
        return Service;
    }

    public void setService(String service) {
        Service = service;
    }
}
