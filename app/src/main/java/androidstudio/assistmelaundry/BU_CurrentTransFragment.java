package androidstudio.assistmelaundry;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by JP on 12/4/2017.
 */

public class BU_CurrentTransFragment extends android.support.v4.app.Fragment {

    //Preference
    SharedPreferences.Editor editor;
    private String USER_TYPE;
    String orderid, orderaddress, ordercontact, ordertimestamp, ordertotal, orderlat, orderlong, orderstat, orderservice, orderpowder, orderpowdertotal, orderfabcon, orderfabcontotal, orderoveralltotal, ordercharge, orderbranchminkg, orderkilocount, orderotherinfo;
    String orderbranch, orderbranchaddress, orderbranchcontact;
    String firebaseorderstatus;
    String ridername,ridercontact,riderready = "false";
    Activity activity;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    MaterialDialog dialog;

    @BindView(R.id.header)
    TextView tvheader;
    @BindView(R.id.tvserviceheader)
    TextView tvserviceheader;
    @BindView(R.id.delordernum)
    TextView tvorderid;
    @BindView(R.id.delordernumhldr)
    TextView tvorderidhldr;
    @BindView(R.id.delchangefor)
    TextView tvorderservice;
    @BindView(R.id.delchangeforhldr)
    TextView tvorderservicehldr;
    @BindView(R.id.delstatus)
    TextView tvorderstatus;
    @BindView(R.id.deltimestamp)
    TextView tvordertimestamp;
    @BindView(R.id.deltimestamphldr)
    TextView tvordertimestamphldr;
    @BindView(R.id.deltotal)
    TextView tvordertotal;
    @BindView(R.id.delpowder)
    TextView tvorderpowder;
    @BindView(R.id.delpowderhldr)
    TextView tvorderpowderhldr;
    @BindView(R.id.delfabcon)
    TextView tvorderfabcon;
    @BindView(R.id.delfabconhldr)
    TextView tvorderfabconhldr;
    @BindView(R.id.buttoncancel)
    FancyButton btnCancel;
//    @BindView(R.id.buttontrackorder)
//    FancyButton btnTrack;
    @BindView(R.id.btn_call)
    CircleImageView btnCall;
    @BindView(R.id.btn_text)
    CircleImageView btnText;
    @BindView(R.id.tvcontactrider)
    TextView tvcontact;
    @BindView(R.id.delotherinfo)
    TextView tvotherinfo;
    @BindView(R.id.delotherinfohldr)
    TextView tvotherinfohldr;
    @BindView(R.id.delservicekilo)
    TextView tvkilocount;
    @BindView(R.id.delservicekilohldr)
    TextView tvkilocounthldr;
    @BindView(R.id.delbranch)
    TextView tvbranch;
    @BindView(R.id.delbranchhldr)
    TextView tvbranchhldr;
    @BindView(R.id.delbranchaddress)
    TextView tvbranchaddress;
    @BindView(R.id.delbranchaddresshldr)
    TextView tvbranchaddresshldr;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookservicefound, container, false);
        ButterKnife.bind(this, view);
        InitializeDesign();
        InitializeFunctions();
        return view;
    }

    public void InitializeDesign(){
        Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Bold.otf");
        Typeface subheaderfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Medium.otf");
        Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Regular.otf");

        tvheader.setTypeface(headerfont);
        tvcontact.setTypeface(headerfont);
        tvserviceheader.setTypeface(headerfont);
        tvorderid.setTypeface(subheaderfont);
        tvorderidhldr.setTypeface(regulartextfont);
        tvorderstatus.setTypeface(subheaderfont);
        tvorderservice.setTypeface(subheaderfont);
        tvorderservicehldr.setTypeface(regulartextfont);
        tvorderpowder.setTypeface(subheaderfont);
        tvorderpowderhldr.setTypeface(regulartextfont);
        tvorderfabcon.setTypeface(subheaderfont);
        tvorderfabconhldr.setTypeface(regulartextfont);
        tvordertimestamp.setTypeface(subheaderfont);
        tvordertimestamphldr.setTypeface(regulartextfont);
        tvordertotal.setTypeface(headerfont);
        tvotherinfo.setTypeface(subheaderfont);
        tvotherinfohldr.setTypeface(regulartextfont);
        tvkilocount.setTypeface(subheaderfont);
        tvkilocounthldr.setTypeface(regulartextfont);
        tvbranch.setTypeface(subheaderfont);
        tvbranchhldr.setTypeface(regulartextfont);
        tvbranchaddress.setTypeface(subheaderfont);
        tvbranchaddresshldr.setTypeface(regulartextfont);

    }

    public void InitializeFunctions(){
        editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
//        btnTrack.setEnabled(true);

        RetrieveBookingData();
        new RetrieveBookingStatus().execute();

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", ridercontact, null));
                startActivity(intent);
            }
        });


        btnText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", ridercontact, null)));
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(riderready.equals("false")){
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.popup_choice, null);
                    dialogBuilder.setView(dialogView);

                    final AlertDialog OptionalertDialog = dialogBuilder.create();
                    Window window = OptionalertDialog.getWindow();
                    window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    window.setGravity(Gravity.CENTER);

                    final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                    final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                    final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                    optionheader.setText("Cancel Booking?");

                    negative.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            OptionalertDialog.dismiss();
                        }
                    });

                    positive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mDatabase.child("Bookings").child(orderid).child("bookingstats").setValue("cancelled");
                            mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("currentorder").setValue("none");

                            Toast.makeText(getActivity(), "You have cancelled your booking", Toast.LENGTH_SHORT).show();
                            editor.putString("booktransaction", "false");
                            editor.putString("bookid", "none");
                            editor.putString("bookaddress", "none");
                            editor.putString("bookcontact", "none");
                            editor.putString("booktimestamp", "none");
                            editor.putString("booktotal", "none");
                            editor.putString("booklatitude", "none");
                            editor.putString("booklongitude", "none");
                            editor.putString("bookcharge", "none");
                            editor.putString("bookkilocount", "none");
                            editor.putString("bookotherinfo", "none");
                            editor.apply();

                            OptionalertDialog.dismiss();

                            Fragment fragment = new BU_MainFragment();
                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.remove(new BU_CurrentTransFragment());
                            fragmentTransaction.replace(R.id.frame_container, fragment);
                            fragmentTransaction.commit();
                        }
                    });
                    OptionalertDialog.show();
                }else{
                    Toast.makeText(getActivity(), "Rider arrived. Cannot cancel anymore", Toast.LENGTH_SHORT).show();
                }
            }
        });
//        btnTrack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), FA_TrackRiderFragment.class);
//                startActivity(intent);
//            }
//        });
    }

    public void RetrieveBookingData(){
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        orderid = prefs.getString("bookid", "ID");
        orderaddress = prefs.getString("bookaddress", "Address");
        ordercontact = prefs.getString("bookcontact", "Contact");
        ordertimestamp = prefs.getString("booktimestamp", "Timestamp");
        orderlat = prefs.getString("booklatitude", "Latitude");
        orderlong = prefs.getString("booklongitude", "Longitude");
        orderstat = prefs.getString("bookstatus", "Status");
        ordercharge = prefs.getString("bookcharge", "DeliveryCharge");
        orderbranchminkg = prefs.getString("bookkilocount", "MinKgCount");
        orderbranch = prefs.getString("bookbranch", "Branch");
        orderbranchaddress = prefs.getString("bookbranchaddress", "BranchAddress");
        orderbranchcontact = prefs.getString("bookbranchcontact", "BranchContact");

        tvbranchhldr.setText(orderbranch);
        tvbranchaddresshldr.setText(orderbranchaddress);

        tvorderidhldr.setText(orderid);
        tvordertimestamphldr.setText(ordertimestamp);
        tvorderstatus.setText("Service would be provided shortly");
        tvorderservicehldr.setText("Wait for Rider Confirmation");
        tvkilocounthldr.setText("Wait for Rider Confirmation");
        tvotherinfohldr.setText("Wait for Rider Confirmation");
        tvorderpowderhldr.setText("Wait for Rider Confirmation");
        tvorderfabconhldr.setText("Wait for Rider Confirmation");
        tvordertotal.setText("Final Total: Wait for Rider Confirmation");
    }

    class RetrieveBookingStatus extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            C_FirebaseClass firebaseFunctions = new C_FirebaseClass(getActivity());
            activity = getActivity();
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Bookings")
                    .child(orderid)
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final C_BookingClass book = dataSnapshot.getValue(C_BookingClass.class);
                            firebaseorderstatus = book.getBookingstatus();
                            ridername = book.getRidername();
                            ridercontact = book.getRidercontact();

                            if (firebaseorderstatus != null && ridername != null && ridercontact != null) {
                                if (firebaseorderstatus.equals("riderarrived") && dialog != null && activity != null) {
                                    dialog = new MaterialDialog.Builder(activity)
                                            .title("Notification")
                                            .content("Rider arrived!")
                                            .positiveText("Close")
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                    riderready = "true";
                                                    tvorderstatus.setText("Rider has arrived. Wait for confirmation");
                                                    dialog = null;
                                                }
                                            })
                                            .show();
                                }

                                if (firebaseorderstatus.equals("orderprocessed") && dialog != null && activity != null) {
                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            dialog = new MaterialDialog.Builder(activity)
                                                    .title("Notification")
                                                    .content("Order has been processed!")
                                                    .positiveText("Close")
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            new RetrieveLaundryData().execute();
                                                            dialog = null;
                                                        }
                                                    })
                                                    .show();
                                        }
                                    }, 4500);
                                }

                                if (firebaseorderstatus.equals("delivery") && dialog != null && activity != null) {
                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            dialog = new MaterialDialog.Builder(activity)
                                                    .title("Notification")
                                                    .content("Laundry Service is done! Rider is on the way back")
                                                    .positiveText("Close")
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            tvorderstatus.setText("Rider is on the way back");
                                                            dialog = null;
                                                        }
                                                    })
                                                    .show();
                                        }
                                    }, 3500);
                                }

                                if (firebaseorderstatus.equals("deliverarrived") && dialog != null && activity != null) {
                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            dialog = new MaterialDialog.Builder(activity)
                                                    .title("Notification")
                                                    .content("Rider has arrived")
                                                    .positiveText("Close")
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                            tvorderstatus.setText("Rider has arrived");
                                                            dialog = null;
                                                        }
                                                    })
                                                    .show();
                                        }
                                    }, 1500);
                                }

                                if (firebaseorderstatus.equals("delivered") && dialog != null && activity != null) {
                                    dialog = new MaterialDialog.Builder(activity)
                                            .title("Service Successful!")
                                            .content("Thank you for using Laundrie!")
                                            .positiveText("Close")
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                    mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("currentorder").setValue("none");

                                                    editor.putString("booktransaction", "false");
                                                    editor.putString("bookid", "none");
                                                    editor.putString("bookaddress", "none");
                                                    editor.putString("bookcontact", "none");
                                                    editor.putString("booktimestamp", "none");
                                                    editor.putString("booktotal", "none");
                                                    editor.putString("booklatitude", "none");
                                                    editor.putString("booklongitude", "none");
                                                    editor.putString("bookservice", "none");
                                                    editor.putString("bookpowder", "none");
                                                    editor.putString("bookfabcon", "none");
                                                    editor.putString("bookoveralltotal", "none");
                                                    editor.putString("bookcharge", "none");
                                                    editor.putString("bookkilocount", "none");
                                                    editor.putString("bookbranch", "none");
                                                    editor.putString("bookbranchaddress", "none");
                                                    editor.putString("bookbranchcontact", "none");
                                                    editor.apply();

                                                    dialog.dismiss();
                                                    dialog = null;

                                                    Fragment fragment = new BU_MainFragment();
                                                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                    fragmentTransaction.remove(new BU_CurrentTransFragment());
                                                    fragmentTransaction.replace(R.id.frame_container, fragment);
                                                    fragmentTransaction.commit();
                                                }
                                            })
                                            .show();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
            return null;
        }
    }

    class RetrieveLaundryData extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            C_FirebaseClass firebaseFunctions = new C_FirebaseClass(getActivity());
            activity = getActivity();
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Bookings")
                    .child(orderid)
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final C_BookingClass book = dataSnapshot.getValue(C_BookingClass.class);
                            orderservice = book.getBookingservice();
                            ordertotal = book.getBookingtotal();
                            orderpowder = book.getBookingpowder();
                            orderpowdertotal = book.getBookingpowdertotal();
                            orderfabcon = book.getBookingfabcon();
                            orderfabcontotal = book.getBookingfabcontotal();
                            orderoveralltotal = book.getBookingoveralltotal();
                            orderkilocount = book.getBookingkilocount();
                            orderotherinfo = book.getBookingotherservices();

                            if (orderservice != null && ordertotal != null && orderpowder != null && orderpowdertotal != null
                                    && orderfabcon != null && orderfabcontotal != null && orderoveralltotal != null && orderkilocount != null && orderotherinfo != null) {
                                tvorderstatus.setText("Order was processed! Just wait for delivery");
                                tvorderservice.setText("The Service you requested: " + orderservice);
                                tvkilocount.setText("Service Kilo count: " + orderkilocount);
                                tvotherinfo.setText("Other Services/Information: " + orderotherinfo);
                                tvorderpowder.setText("The Powder you selected: " + orderpowder);
                                tvorderfabcon.setText("The Fabric Conditioner you selected: " + orderfabcon);
                                displayFinalTotal();

                                SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                editor.putString("bookservice", orderservice);
                                editor.putString("bookpowder", orderpowder);
                                editor.putString("bookfabcon", orderfabcon);
                                editor.putString("bookoveralltotal", orderoveralltotal);
                                editor.putString("bookstatus", "processed");
                                editor.putString("bookkilocount", orderkilocount);
                                editor.putString("bookotherinfo", orderotherinfo);
                                editor.apply();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
            return null;
        }
    }


    public void displayFinalTotal(){
        tvordertotal.setText("₱"+(Double.valueOf(orderoveralltotal) + Double.valueOf(ordercharge))+".00");
//        SpannableStringBuilder builder = new SpannableStringBuilder();
//        SpannableString str1= new SpannableString("Service Total(Per "+orderbranchminkg+" Kilos): ");
//        str1.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str1.length(), 0);
//        builder.append(str1);
//
//        SpannableString str2= new SpannableString( "₱"+ordertotal+".00"+"\n");
//        str2.setSpan(new ForegroundColorSpan(Color.RED), 0, str2.length(), 0);
//        builder.append(str2);
//
//        SpannableString str3= new SpannableString("Powder Price Total: ");
//        str3.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str3.length(), 0);
//        builder.append(str3);
//
//        SpannableString str4= new SpannableString( "₱"+orderpowdertotal+".00"+"\n");
//        str4.setSpan(new ForegroundColorSpan(Color.RED), 0, str4.length(), 0);
//        builder.append(str4);
//
//        SpannableString str5= new SpannableString("FabCon Price Total: ");
//        str5.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str5.length(), 0);
//        builder.append(str5);
//
//        SpannableString str6= new SpannableString( "₱"+orderfabcontotal+".00"+"\n");
//        str6.setSpan(new ForegroundColorSpan(Color.RED), 0, str6.length(), 0);
//        builder.append(str6);
//
//        SpannableString str7= new SpannableString("Service Charge: ");
//        str7.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str7.length(), 0);
//        builder.append(str7);
//
//        SpannableString str8= new SpannableString( "₱"+ordercharge+".00"+"\n");
//        str8.setSpan(new ForegroundColorSpan(Color.RED), 0, str8.length(), 0);
//        builder.append(str8);
//
//        SpannableString str9= new SpannableString("Approximate Final Payout: ");
//        str9.setSpan(new StyleSpan(Typeface.BOLD), 0, str9.length(), 0);
//        str9.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str9.length(), 0);
//        builder.append(str9);
//
//        SpannableString str10= new SpannableString( "₱"+orderoveralltotal+".00");
//        str10.setSpan(new StyleSpan(Typeface.BOLD), 0, str10.length(), 0);
//        str10.setSpan(new ForegroundColorSpan(Color.RED), 0, str10.length(), 0);
//        builder.append(str10);
//        tvordertotal.setText(builder, TextView.BufferType.SPANNABLE);
    }
}
