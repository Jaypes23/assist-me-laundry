package androidstudio.assistmelaundry;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;

public class FA_LoginActivity extends AppCompatActivity {


    //EditText
    @BindView(R.id.login_username)
    EditText etUserId;
    @BindView(R.id.login_password)
    EditText etPassword;
    @BindView(R.id.login_usernametextinput)
    TextInputLayout tlusername;
    @BindView(R.id.login_passwordtextinput)
    TextInputLayout tlpassword;

    //TextViews
    @BindView(R.id.tvSignUp)
    TextView tvsignup;
    @BindView(R.id.tvForgot)
    TextView tvforgotpassword;
    @BindView(R.id.tvfblogin)
    TextView tvfblogin;

    //Button
    @BindView(R.id.btnLogin)
    FancyButton btnLogin;
    @BindView(R.id.facebookLogin)
    FancyButton btnFB;

    MaterialDialog inputQuan;

    //Variables
    private String userstatus,usertype, username, useremail, useraddress, usercontactnum, usercurrentorder;
    private String fbuid, fbname, fbemail;
    private String isrider = "false";
    private String ridername, rideremail, riderlaundry, riderstatus,riderbranchid;
    private String bookid, bookaddress, bookcontact, bookcustname, bookstatus, booklat, booklong, bookttimestamp, bookkilocount, bookotherinfo;
    private String bookcharge;
    private String branchname, branchid, branchmainid, branchminkgcount, branchriderid;
    private long ridercontact;

    //Preference
    private String USER_TYPE;
    private String statushldr;
    private int timehldr;

    //MaterialDialog
    private MaterialDialog logindialog,fblogindialog;

    //Firebase
    private C_FirebaseClass firebasefunctions;
    private DatabaseReference mRootRef;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    private CallbackManager callbackManager;
    public static final String TAG = "LoginActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        InitializeDesign();
        InitializeFunctions();
    }

    public void InitializeDesign(){
        Typeface headerfont = Typeface.createFromAsset(getAssets(),  "fonts/Poppins-Bold.otf");
        Typeface subheaderfont = Typeface.createFromAsset(getAssets(),  "fonts/Poppins-Medium.otf");
        Typeface regulartextfont = Typeface.createFromAsset(getAssets(),  "fonts/Poppins-Regular.otf");

        tvsignup.setTypeface(regulartextfont);
        tvforgotpassword.setTypeface(regulartextfont);
        tvfblogin.setTypeface(regulartextfont);
        tlusername.setTypeface(subheaderfont);
        tlpassword.setTypeface(subheaderfont);
        etUserId.setTypeface(subheaderfont);
        etPassword.setTypeface(subheaderfont);
    }

    public void InitializeFunctions(){
        mAuth = FirebaseAuth.getInstance();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Toast.makeText(FA_LoginActivity.this, "Facebook Authentication Successful! Please wait..", Toast.LENGTH_SHORT).show();
                fblogindialog = new MaterialDialog.Builder(FA_LoginActivity.this)
                        .title("Validating data")
                        .content("Please wait...")
                        .progress(true, 0)
                        .show();
                FBLogin(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(FA_LoginActivity.this, "FB Login Cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(FA_LoginActivity.this, String.valueOf(exception), Toast.LENGTH_SHORT).show();
            }
        });

        btnFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager
                        .getInstance().
                        logInWithReadPermissions(FA_LoginActivity.this, Arrays.asList("public_profile", "email"));
            }
        });

        tvsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FA_LoginActivity.this, FA_RegisterActivity.class);
                intent.putExtra("user_id","default");
                intent.putExtra("user_name", "default");
                intent.putExtra("user_email", "default");
                startActivity(intent);
            }
        });

        tvforgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 inputQuan = new MaterialDialog.Builder(FA_LoginActivity.this)
                        .title("Forgot your password?")
                        .content("Input your email")
                        .inputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
                        .input("Email", "", new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                String forgetemail = String.valueOf(inputQuan.getInputEditText().getText());
                                if (TextUtils.isEmpty(inputQuan.getInputEditText().getText())) {
                                    Toast.makeText(getApplication(), "Enter your registered email id", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                mAuth.sendPasswordResetEmail(forgetemail)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Toast.makeText(FA_LoginActivity.this, "We have sent you instructions to reset your password!", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(FA_LoginActivity.this, "Failed to send reset email!", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                            }
                        })
                        .positiveText("Proceed")
                        .neutralText("Cancel")
                        .cancelable(false)
                        .show();
            }
        });

        firebasefunctions = new C_FirebaseClass(this);
        mRootRef = FirebaseDatabase.getInstance().getReference();
        mAuth = firebasefunctions.getUserInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                SharedPreferences prefs = getSharedPreferences(USER_TYPE, MODE_PRIVATE);
                statushldr = prefs.getString("status", "Welcome");
                timehldr = prefs.getInt("timehldr", 0);
                if(timehldr == 0){
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            SharedPreferences prefs = getSharedPreferences(USER_TYPE, MODE_PRIVATE);
                            statushldr = prefs.getString("status", "Welcome");
                            if(statushldr.equals("user")){
                                Intent intent = new Intent(FA_LoginActivity.this, FA_MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            if(statushldr.equals("client")){
                                Intent intent = new Intent(FA_LoginActivity.this, FA_MainClientActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            if(statushldr.equals("Welcome")){
                                Log.w("Login", "Welcome");
                            }
                        }
                    }, 3000);
                }

                else{
                    if(statushldr.equals("user")){
                        Intent intent = new Intent(FA_LoginActivity.this, FA_MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    if(statushldr.equals("client")){
                        Intent intent = new Intent(FA_LoginActivity.this, FA_MainClientActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    if(statushldr.equals("Welcome")){
                        Log.w("Login", "Welcome");
                    }
                }
            }
        };
    }

    public void Login(View v) {
        logindialog = new MaterialDialog.Builder(this)
                .title("Logging in")
                .content("Please wait...")
                .progress(true, 0)
                .show();
        if (TextUtils.isEmpty(etUserId.getText().toString())) {
            etUserId.setError("Enter email address");
            logindialog.dismiss();
        } else if (TextUtils.isEmpty(etPassword.getText().toString())) {
            etPassword.setError("Enter password");
            logindialog.dismiss();
        } else if (!isValidEmail(etUserId.getText().toString())) {
            etUserId.setError("Enter a valid email address");
            logindialog.dismiss();
        } else {
            mAuth.signInWithEmailAndPassword(etUserId.getText().toString(), etPassword.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull final Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                logindialog.dismiss();
                                try {
                                    throw task.getException();

                                } catch (FirebaseAuthInvalidUserException e) {
                                    new MaterialDialog.Builder(FA_LoginActivity.this)
                                            .title("Login Failed")
                                            .content("User Not Found")
                                            .positiveText("Close")
                                            .show();
                                    etPassword.setText("");
                                } catch (FirebaseAuthInvalidCredentialsException e) {
                                    new MaterialDialog.Builder(FA_LoginActivity.this)
                                            .title("Login Failed")
                                            .content("Email or Password did not match. Please try again")
                                            .positiveText("Close")
                                            .show();
                                    etPassword.setText("");
                                } catch (FirebaseNetworkException e) {
                                    new MaterialDialog.Builder(FA_LoginActivity.this)
                                            .title("Login Failed")
                                            .content("Please Check Internet Connection, and try again")
                                            .positiveText("Close")
                                            .show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else {
                                //LOGIN SUCCESSFUL
                                new RetrieveUserStatus().execute();
                            }
                        }
                    });
        }
    }

    public boolean isValidEmail(CharSequence target) {
        return target != null && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    class RetrieveUserStatus extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            C_FirebaseClass firebaseFunctions = new C_FirebaseClass(FA_LoginActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Users")
                            .child(mAuth.getCurrentUser().getUid())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.getValue() == null){
                                        new RetrieveRiderStatus().execute();
                                        new RetrieveBookingInfoRider().execute();
                                    }else {
                                        final C_UserClass user = dataSnapshot.getValue(C_UserClass.class);
                                        userstatus = user.getUserstatus();
                                        username = user.getName();
                                        useremail = user.getEmail();
                                        useraddress = user.getAddress();
                                        usercontactnum = user.getContact();
                                        usercurrentorder = user.getCurrentorder();

                                        if (userstatus != null && username != null && useremail != null && useraddress != null && usercontactnum != null && usercurrentorder != null) {
                                            if (userstatus.equals("user")) {
                                                usertype = "user";

                                                if(!usercurrentorder.equals("none")){
                                                    new RetrieveBookingInfo().execute();
                                                }

                                                SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                                editor.putString("booktransaction", "false");
                                                editor.putString("userid", mAuth.getCurrentUser().getUid());
                                                editor.putString("status", "user");
                                                editor.putString("username", username);
                                                editor.putString("useremail", useremail);
                                                editor.putString("useraddress", useraddress);
                                                editor.putString("usercontact", usercontactnum);
                                                editor.putInt("timehldr", 3000);
                                                editor.apply();
                                            }
                                        }
                                    }

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }

    class RetrieveRiderStatus extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            C_FirebaseClass firebaseFunctions = new C_FirebaseClass(FA_LoginActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Rider").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mRootRef.child("Rider")
                            .child(mAuth.getCurrentUser().getUid())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final C_RiderClass rider = dataSnapshot.getValue(C_RiderClass.class);
                                    ridername = rider.getName();
                                    ridercontact = rider.getContactNo();
                                    riderstatus = rider.getStatus();
                                    rideremail = rider.getEmail();
                                    riderlaundry = rider.getLaundryShop();
                                    riderbranchid = rider.getBranchId();

                                    if (riderstatus != null && ridername != null && rideremail != null && riderlaundry != null && String.valueOf(ridercontact) != null) {
                                        if (riderstatus.equals("Active")) {
                                            usertype = "client";
                                            SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                            editor.putString("booktransaction", "false");
                                            editor.putString("userid", mAuth.getCurrentUser().getUid());
                                            editor.putString("status", "client");
                                            editor.putString("username", ridername);
                                            editor.putString("useremail", rideremail);
                                            editor.putString("useraddress", riderlaundry);
                                            editor.putString("usercontact", String.valueOf(ridercontact));
                                            editor.putString("riderbranchid", riderbranchid);
                                            editor.putInt("timehldr", 3000);
                                            editor.apply();
                                        } else {
                                            new MaterialDialog.Builder(FA_LoginActivity.this)
                                                    .title("Login Failed")
                                                    .content("Your Rider account is not yet active. Please wait for activation")
                                                    .positiveText("Close")
                                                    .show();
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void FBLogin(AccessToken token){
        final AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            fbuid = user.getUid();
                            fbname = user.getDisplayName();
                            fbemail = user.getEmail();

                            if(fbuid !=null && fbname !=null && fbemail !=null){
                                new RetrieveUserStatusFB().execute();
                            }

                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(FA_LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                            try {
                                throw task.getException();
                            } catch (FirebaseAuthUserCollisionException e) {
                                new MaterialDialog.Builder(FA_LoginActivity.this)
                                        .title("Login Failed")
                                        .content("An account already exists with the same email address. Try Sign in using email address")
                                        .positiveText("Close")
                                        .show();
                                etPassword.setText("");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }

    class RetrieveUserStatusFB extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            C_FirebaseClass firebaseFunctions = new C_FirebaseClass(FA_LoginActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.hasChild(fbuid)){
                        mRootRef.child("Users")
                                .child(fbuid)
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        final C_UserClass user = dataSnapshot.getValue(C_UserClass.class);
                                        userstatus = user.getUserstatus();
                                        username = user.getName();
                                        useremail = user.getEmail();
                                        useraddress = user.getAddress();
                                        usercontactnum = user.getContact();
                                        usercurrentorder = user.getCurrentorder();

                                        if (userstatus != null && username != null && useremail != null && useraddress != null && usercontactnum != null && usercurrentorder != null) {
                                            if (userstatus.equals("user")) {
                                                usertype = "user";

                                                if(!usercurrentorder.equals("none")){
                                                    new RetrieveBookingInfo().execute();
                                                }

                                                SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                                editor.putString("booktransaction", "false");
                                                editor.putString("status", "user");
                                                editor.putString("userid", fbuid);
                                                editor.putString("username", username);
                                                editor.putString("useremail", useremail);
                                                editor.putString("useraddress", useraddress);
                                                editor.putString("usercontact", usercontactnum);
                                                editor.putInt("timehldr", 3000);
                                                editor.apply();

                                                fblogindialog.dismiss();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                    }
                    else{
                        fblogindialog.dismiss();
                        Intent intent = new Intent(getApplicationContext(), FA_RegisterActivity.class);
                        intent.putExtra("user_id",fbuid);
                        intent.putExtra("user_name", fbname);
                        intent.putExtra("user_email", fbemail);
                        startActivity(intent);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }

    class RetrieveBookingInfo extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            C_FirebaseClass firebaseFunctions = new C_FirebaseClass(FA_LoginActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Bookings")
                    .child(usercurrentorder)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final C_BookingClass book = dataSnapshot.getValue(C_BookingClass.class);
                    String bookingstatus = book.getBookingstatus();
                    SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();

                    if (bookingstatus != null) {
                        if (bookingstatus.equals("orderprocessed") || bookingstatus.equals("delivery") || bookingstatus.equals("deliverarrived") || bookingstatus.equals("delivered")) {
                            editor.putString("bookservice", book.getBookingservice());
                            editor.putString("bookpowder", book.getBookingpowder());
                            editor.putString("bookfabcon", book.getBookingfabcon());
                            editor.putString("bookoveralltotal", book.getBookingtotal());
                            editor.putString("bookkilocount", book.getBookingkilocount());
                            editor.putString("bookotherinfo", book.getBookingotherservices());
                        }

                        editor.putString("booktransaction", "true");
                        editor.putString("bookid", book.getBookingid());
                        editor.putString("bookcontact", book.getBookingcontact());
                        editor.putString("bookaddress", book.getBookingaddress());
                        editor.putString("booktimestamp", book.getBookingtimestamp());
                        editor.putString("booklatitude", book.getBookinglat());
                        editor.putString("booklongitude", book.getBookinglong());
                        editor.putString("bookcharge", book.getBookingcharge());
                        editor.putString("bookkilocount", book.getBookingkilocount());
                        editor.putString("bookbranch", book.getBranchname());
                        editor.putString("bookbranchaddress", book.getBranchaddress());
                        editor.putString("bookbranchcontact", book.getBranchcontact());
                        editor.putString("bookstatus", book.getBookingstatus());
                        editor.apply();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            return null;
        }
    }

    class RetrieveBookingInfoRider extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            C_FirebaseClass firebaseFunctions = new C_FirebaseClass(FA_LoginActivity.this);
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Bookings")
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final C_BookingClass book = dataSnapshot.getValue(C_BookingClass.class);
                            bookcustname = book.getBookingcustname();
                            bookid = book.getBookingid();
                            bookaddress = book.getBookingaddress();
                            bookcontact = book.getBookingcontact();
                            bookstatus = book.getBookingstatus();
                            bookttimestamp = book.getBookingtimestamp();
                            booklat = book.getBookinglat();
                            booklong = book.getBookinglong();
                            bookcharge = book.getBookingcharge();
                            branchriderid = book.getRiderid();
                            branchname = book.getBranchname();
                            branchid = book.getBranchid();
                            branchmainid = book.getMainbranchid();
                            bookkilocount = book.getBookingkilocount();
                            bookotherinfo = book.getBookingotherservices();
                            branchminkgcount = book.getBranchminkgcount();

                            if(bookcustname !=null && bookid !=null && bookaddress !=null && bookcontact !=null && bookstatus !=null
                                    && bookttimestamp !=null && booklat != null && booklong !=null && bookcharge !=null  && branchriderid !=null
                                    && branchname !=null && branchid !=null && branchmainid !=null && bookkilocount !=null && bookotherinfo !=null && branchminkgcount !=null) {

                                if(branchriderid.equals(mAuth.getCurrentUser().getUid())){
                                    if(bookstatus.equals("otw") || bookstatus.equals("riderarrived") || bookstatus.equals("delivery")){
                                        SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                        editor.putString("booktransaction", "true");
                                        editor.putString("bookid", bookid);
                                        editor.putString(bookid + "bookcustname", bookcustname);
                                        editor.putString(bookid + "bookcontact", bookcontact);
                                        editor.putString(bookid + "bookaddress", bookaddress);
                                        editor.putString(bookid + "booktimestamp", bookttimestamp);
                                        editor.putString(bookid + "booklatitude", booklat);
                                        editor.putString(bookid + "booklongitude", booklong);
                                        editor.putString(bookid + "bookcharge", bookcharge);
                                        editor.putString(bookid + "branchname", branchname);
                                        editor.putString(bookid + "branchid", branchid);
                                        editor.putString(bookid + "branchmainid", branchmainid);
                                        editor.putString(bookid + "bookingotherservices", bookkilocount);
                                        editor.putString(bookid + "bookingkilocount", bookotherinfo);
                                        editor.putString(bookid + "branchminkgcount", branchminkgcount);
                                        if(bookstatus.equals("delivery")){
                                            editor.putString(bookid + "bookstatus", "deliverytrue");
                                        }else{
                                            editor.putString(bookid + "bookstatus", "matched");
                                        }
                                        editor.apply();
                                    }
                                    else if (bookstatus.equals("orderprocessed")){
                                        SharedPreferences.Editor editor = getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                        editor.putString(bookid + "bookcustname", bookcustname);
                                        editor.putString(bookid + "bookcontact", bookcontact);
                                        editor.putString(bookid + "bookaddress", bookaddress);
                                        editor.putString(bookid + "booktimestamp", bookttimestamp);
                                        editor.putString(bookid + "booklatitude", booklat);
                                        editor.putString(bookid + "booklongitude", booklong);
                                        editor.putString(bookid + "bookcharge", bookcharge);
                                        editor.putString(bookid + "branchname", branchname);
                                        editor.putString(bookid + "branchid", branchid);
                                        editor.putString(bookid + "branchmainid", branchmainid);
                                        editor.putString(bookid + "bookingotherservices", bookkilocount);
                                        editor.putString(bookid + "bookingkilocount", bookotherinfo);
                                        editor.putString(bookid + "branchminkgcount", branchminkgcount);
                                        editor.putString(bookid + "bookstatus", "matched");
                                        editor.apply();
                                    }
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
            return null;
        }
    }



}