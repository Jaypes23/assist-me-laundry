package androidstudio.assistmelaundry;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.fancybuttons.FancyButton;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by JP on 12/4/2017.
 */

public class BU_MainFragment extends android.support.v4.app.Fragment
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    //UI
    @BindView(R.id.llmainfragment)
    RelativeLayout rlmain;
    @BindView(R.id.checkout_address)
    EditText etAddress;
    @BindView(R.id.tlAddress)
    TextInputLayout tlAddress;
    @BindView(R.id.checkout_contact)
    EditText etContact;
    @BindView(R.id.tlContact)
    TextInputLayout tlContact;
    @BindView(R.id.spinnerlaundry)
    MaterialSpinner mslaundryshop;

    @BindView(R.id.tvheader)
    TextView tvheader;
    @BindView(R.id.headerinfo)
    TextView tvheaderinfo;
    @BindView(R.id.tvmenu)
    TextView tvmenu;
    @BindView(R.id.tvlaundryinfoheader)
    TextView tvlaundryinfoheader;
    @BindView(R.id.tvlaundryinfo)
    TextView tvlaundryinfo;
    @BindView(R.id.tvmenuheader)
    TextView tvmenuheader;
    @BindView(R.id.overalltotal)
    TextView tvoveralltotal;
    @BindView(R.id.buttonfind)
    FancyButton btnBook;
    MaterialDialog dialog;


    //Preference
    private String USER_TYPE;
    String ordertransaction, uid, name, email, address, contact;
    String ordernum;
    String reportDate;
    String servicefee;
    String status;

    //Firebase
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    ArrayList <String> branchnamearr;
    ArrayList <String> branchinfoarr;
    ArrayList <String> serviceshldr;
    ArrayList<ArrayList<String>> servicesarr;
    ArrayList <String> serviceprovided;

    //Location Variables
    String faddress;
    Geocoder geocoder;
    List<Address> addresses;
    String latitude, longitude;
    boolean GPSStatus;
    LocationManager locationManager;
    public Criteria criteria;
    public String bestProvider;
    int EARTH_RADIUS_KM = 6371;
    double lat1,lon1,lat2,lon2,lat1Rad,lat2Rad,deltaLonRad,dist;

    private String laundryname;
    private String branchaddress;
    private String branchcontactno;
    private String branchcontactperson;
    private String branchmainid;
    private String branchstatus;
    private Double branchlat;
    private Double branchlong;
    private int branchkilocount;

    String Lname, Laddress, Lcontactnumber, Lcontactperson, Lkilocount, Lmainbranchid, Lbranchid, Lservicefee;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookservice, container, false);
        ButterKnife.bind(this, view);
        InitializeDesign();
        InitializeFunctions();
        return view;
    }

    public void InitializeDesign(){
        Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Bold.otf");
        Typeface regulartextfont = Typeface.createFromAsset(getActivity().getAssets(),  "fonts/Poppins-Regular.otf");

        tvheader.setTypeface(headerfont);
        tvmenuheader.setTypeface(headerfont);
        tvlaundryinfoheader.setTypeface(headerfont);
        tvlaundryinfo.setTypeface(regulartextfont);
        tvheaderinfo.setTypeface(regulartextfont);
        tvmenu.setTypeface(regulartextfont);
        tvoveralltotal.setTypeface(regulartextfont);
        etAddress.setTypeface(regulartextfont);
        etContact.setTypeface(regulartextfont);
        tlAddress.setTypeface(regulartextfont);
        tlContact.setTypeface(regulartextfont);
        mslaundryshop.setTypeface(regulartextfont);
    }

    public void InitializeFunctions() {
        RetrieveUserData();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        branchinfoarr = new ArrayList<>();
        branchnamearr = new ArrayList<>();
        serviceshldr = new ArrayList<String>();
        servicesarr = new ArrayList<>();

        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        String orderstat = prefs.getString("bookstatus", "Status");
        if(orderstat.equals("searching")){
            RiderFindStatus();
        }

        //---------------------------------Check if Internet Connection exists-----------------------------//
        if (haveNetworkConnection(getActivity())) {
            //---------------------------------Check if GPS is turned on-----------------------------//
                getLocation();
                branchnamearr.add("Select Laundry Shop");
                new RetrieveLaundryShops().execute();
                //---------------------------------Check if Laundry Shops are retrieved-----------------------------//
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if(serviceshldr.isEmpty()){
                            displayPromptForEnablingNetwork(getActivity(), "Check Internet Connection. There might be no Internet Access or Internet is slow");
                        }
                    }
                }, 5000);

                etAddress.setText(address);
                etContact.setText(contact);
                tvlaundryinfo.setText("No Laundry Shop selected yet");
                tvmenu.setText("No Laundry Shop selected yet");
                tvoveralltotal.setText("Note: Final Total will be shown once a Rider confirms your order");

                btnBook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CheckGpsStatus();
                        if (GPSStatus != true) {
                            Toast.makeText(getActivity(), "GPS Should be Turned on before you can proceed", Toast.LENGTH_SHORT).show();
                            EnableGPSAutoMatically();
                        } else {
                            if (TextUtils.isEmpty(etAddress.getText().toString())) {
                                etAddress.setError("Required");
                                Snackbar snackbar = Snackbar.make(rlmain, "Address is required", Snackbar.LENGTH_LONG);
                                snackbar.show();
                                return;
                            }
                            if (TextUtils.isEmpty(etContact.getText().toString())) {
                                etContact.setError("Required");
                                Snackbar snackbar = Snackbar.make(rlmain, "Contact # is required", Snackbar.LENGTH_LONG);
                                snackbar.show();
                                return;
                            }
                            if (mslaundryshop.getText().toString().equals("Select Laundry Shop")) {
                                Snackbar snackbar = Snackbar.make(rlmain, "Please select a Laundry Shop first", Snackbar.LENGTH_LONG);
                                snackbar.show();
                            } else {

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                                LayoutInflater inflater = getActivity().getLayoutInflater();
                                View dialogView = inflater.inflate(R.layout.popup_choice, null);
                                dialogBuilder.setView(dialogView);

                                final AlertDialog OptionalertDialog = dialogBuilder.create();
                                Window window = OptionalertDialog.getWindow();
                                window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                window.setGravity(Gravity.CENTER);

                                final Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Poppins-Bold.otf");

                                final TextView optionheader = (TextView) dialogView.findViewById(R.id.optionheader);
                                final FancyButton negative = (FancyButton) dialogView.findViewById(R.id.btnNegative);
                                final FancyButton positive = (FancyButton) dialogView.findViewById(R.id.btnPositive);

                                optionheader.setTypeface(headerfont);
                                optionheader.setText("Book Now?");

                                negative.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        OptionalertDialog.dismiss();
                                    }
                                });

                                positive.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        //---------------------------------Finalizing Booking & Inputting in Database-----------------------------//
                                        OptionalertDialog.dismiss();
                                        getLocation();
                                        Date currentTime = Calendar.getInstance().getTime();
                                        DateFormat df = new SimpleDateFormat("MM-dd-yyyy/HH:mm");
                                        reportDate = df.format(currentTime);
                                        final int random = new Random().nextInt(991) + 10;
                                        ordernum = "SER-" + uid.substring(20) + random;

                                        C_BookingClass book = new C_BookingClass(mAuth.getCurrentUser().getUid(), ordernum, name, etAddress.getText().toString(), etContact.getText().toString(),
                                                "NR Yet", "pending", reportDate, latitude, longitude, "NR Yet", "none", "NR Yet", "NR Yet",
                                                "NR Yet", "NR Yet", "NR Yet", "NR Yet", "NR Yet", Lservicefee, mslaundryshop.getText().toString(), Laddress, Lcontactnumber, Lkilocount, Lbranchid, Lmainbranchid, "none", "none");
                                        mDatabase.child("Bookings").child(ordernum).setValue(book);
                                        mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("currentorder").setValue(ordernum);

                                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
                                        LayoutInflater inflater = getActivity().getLayoutInflater();
                                        View dialogView = inflater.inflate(R.layout.popup_findingmatch, null);
                                        dialogBuilder.setView(dialogView);

                                        final AlertDialog matchDialog = dialogBuilder.create();
                                        Window window = matchDialog.getWindow();
                                        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                        window.setGravity(Gravity.CENTER);

                                        final TextView tvheader = (TextView) dialogView.findViewById(R.id.header);

                                        tvheader.setTypeface(headerfont);

                                        final PulsatorLayout pulsator = (PulsatorLayout) dialogView.findViewById(R.id.pulsator);
                                        pulsator.start();

                                        final FancyButton btnMatching = (FancyButton) dialogView.findViewById(R.id.btnMatching);
                                        btnMatching.setText("Cancel Booking");

                                        //---------------------------------If Booking is Cancelled-----------------------------//
                                        btnMatching.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                matchDialog.dismiss();
                                                mDatabase.child("Bookings").child(ordernum).removeValue();
                                                mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("currentorder").setValue("none");

                                                SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                                editor.putString("booktransaction", "false");
                                                editor.putString("bookid", "none");
                                                editor.putString("bookcontact", "none");
                                                editor.putString("bookaddress", "none");
                                                editor.putString("booktimestamp", "none");
                                                editor.putString("booklatitude", "none");
                                                editor.putString("booklongitude", "none");
                                                editor.putString("bookcharge", "none");
                                                editor.putString("bookkilocount", "none");
                                                editor.putString("bookbranch", "none");
                                                editor.putString("bookbranchaddress", "none");
                                                editor.putString("bookbranchcontact", "none");
                                                editor.putString("bookstatus", "none");
                                                editor.apply();

                                                dialog = new MaterialDialog.Builder(getActivity())
                                                        .title("Booking Cancelled!")
                                                        .content("You have cancelled your booking")
                                                        .positiveText("Close")
                                                        .show();
                                            }
                                        });

                                        matchDialog.show();
                                        SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                        editor.putString("booktransaction", "false");
                                        editor.putString("bookid", ordernum);
                                        editor.putString("bookcontact", etContact.getText().toString());
                                        editor.putString("bookaddress", etAddress.getText().toString());
                                        editor.putString("booktimestamp", reportDate);
                                        editor.putString("booklatitude", latitude);
                                        editor.putString("booklongitude", longitude);
                                        editor.putString("bookcharge", Lservicefee);
                                        editor.putString("bookkilocount", Lkilocount);
                                        editor.putString("bookbranch", Lname);
                                        editor.putString("bookbranchaddress", Laddress);
                                        editor.putString("bookbranchcontact", Lcontactnumber);
                                        editor.putString("bookstatus", "searching");
                                        editor.apply();

                                        //---------------------------------Checking if Rider is found-----------------------------//
                                        C_FirebaseClass firebaseFunctions = new C_FirebaseClass(getActivity());
                                        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
                                        mRootRef.child("Bookings")
                                                .child(ordernum)
                                                .addChildEventListener(new ChildEventListener() {
                                                    @Override
                                                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                                    }

                                                    @Override
                                                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                                                        if (String.valueOf(dataSnapshot.getKey()).equals("bookingstatus")) {
                                                            status = String.valueOf(dataSnapshot.getValue());
                                                            if (status.equals("otw")) {
                                                                matchDialog.dismiss();
                                                                btnMatching.setEnabled(false);
                                                                btnMatching.setText("Finding Finished");
                                                                dialog = new MaterialDialog.Builder(getActivity())
                                                                        .title("Booking Update!")
                                                                        .content("We have found you a rider!")
                                                                        .positiveText("Proceed")
                                                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                                            @Override
                                                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                                                SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                                                                editor.putString("booktransaction", "true");
                                                                                editor.putString("bookstatus", "matched");
                                                                                editor.apply();

                                                                                Fragment fragment = new BU_CurrentTransFragment();
                                                                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                                                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                                                fragmentTransaction.replace(R.id.frame_container, fragment);
                                                                                fragmentTransaction.remove(new BU_MainFragment());
                                                                                fragmentTransaction.commit();
                                                                            }
                                                                        })
                                                                        .show();
                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                                                        matchDialog.dismiss();
                                                    }

                                                    @Override
                                                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                });

                                    }
                                });
                                OptionalertDialog.show();
                            }
                        }
                    }
                });
        }else{
            displayPromptForEnablingNetwork(getActivity(), "Check Internet Connection first!");
        }
    }

    public void RiderFindStatus(){
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        final String orderid = prefs.getString("bookid", "ID");

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.popup_findingmatch, null);
        dialogBuilder.setView(dialogView);

        final AlertDialog matchDialog = dialogBuilder.create();
        Window window = matchDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        final Typeface headerfont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Poppins-Bold.otf");
        final TextView tvheader = (TextView) dialogView.findViewById(R.id.header);
        final PulsatorLayout pulsator = (PulsatorLayout) dialogView.findViewById(R.id.pulsator);
        final FancyButton btnMatching = (FancyButton) dialogView.findViewById(R.id.btnMatching);

        tvheader.setTypeface(headerfont);
        pulsator.start();
        btnMatching.setText("Cancel Booking");

        //---------------------------------If Booking is Cancelled-----------------------------//
        btnMatching.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                matchDialog.dismiss();
                mDatabase.child("Bookings").child(orderid).removeValue();
                mDatabase.child("Users").child(mAuth.getCurrentUser().getUid()).child("currentorder").setValue("none");

                SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                editor.putString("booktransaction", "false");
                editor.putString("bookid", "none");
                editor.putString("bookcontact", "none");
                editor.putString("bookaddress", "none");
                editor.putString("booktimestamp", "none");
                editor.putString("booklatitude", "none");
                editor.putString("booklongitude", "none");
                editor.putString("bookcharge", "none");
                editor.putString("bookkilocount", "none");
                editor.putString("bookbranch", "none");
                editor.putString("bookbranchaddress", "none");
                editor.putString("bookbranchcontact", "none");
                editor.putString("bookstatus", "none");
                editor.apply();

                dialog = new MaterialDialog.Builder(getActivity())
                        .title("Booking Cancelled!")
                        .content("You have cancelled your booking")
                        .positiveText("Close")
                        .show();
            }
        });

        matchDialog.show();

        C_FirebaseClass firebaseFunctions = new C_FirebaseClass(getActivity());
        final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
        mRootRef.child("Bookings")
                .child(orderid)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        if (String.valueOf(dataSnapshot.getKey()).equals("bookingstatus")) {
                            String ordstatus = String.valueOf(dataSnapshot.getValue());
                            if (ordstatus.equals("otw")) {
                                SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                editor.putString("booktransaction", "true");
                                editor.putString("bookstatus", "matched");
                                editor.apply();

                                Fragment fragment = new BU_CurrentTransFragment();
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.frame_container, fragment);
                                fragmentTransaction.remove(new BU_MainFragment());
                                fragmentTransaction.commit();
                            }
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        if (String.valueOf(dataSnapshot.getKey()).equals("bookingstatus")) {
                            String ordstatus = String.valueOf(dataSnapshot.getValue());
                            if (ordstatus.equals("otw")) {
                                SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE).edit();
                                editor.putString("booktransaction", "true");
                                editor.putString("bookstatus", "matched");
                                editor.apply();

                                Fragment fragment = new BU_CurrentTransFragment();
                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.frame_container, fragment);
                                fragmentTransaction.remove(new BU_MainFragment());
                                fragmentTransaction.commit();
                            }
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {}

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(getActivity(), "Suspended", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
    }


    class RetrieveLaundryShops extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... voids) {
            C_FirebaseClass firebaseFunctions = new C_FirebaseClass(getActivity());
            final DatabaseReference mRootRef = firebaseFunctions.getDatabaseReference();
            mRootRef.child("Branch")
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for(final DataSnapshot branchsnapshot : dataSnapshot.getChildren()){
                                mRootRef.child("Branch")
                                        .child(branchsnapshot.getKey())
                                        .addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot branchsnapshot) {
                                                final C_BranchClass branch = branchsnapshot.getValue(C_BranchClass.class);
                                                laundryname = branch.getLaundryName();
                                                branchaddress = branch.getAddress();
                                                branchcontactno = branch.getContactNo();
                                                branchcontactperson = branch.getContactPerson();
                                                branchstatus = branch.getStatus();
                                                branchlat = branch.getLat();
                                                branchlong = branch.getLng();
                                                branchkilocount = branch.getMinWeight();
                                                serviceshldr = branch.getService();
                                                branchmainid = branch.getLaundryShop();
                                                servicefee = branch.getServiceFee();

                                                if(laundryname !=null && branchaddress !=null && branchcontactno !=null && branchcontactperson !=null &&
                                                        branchstatus !=null && branchlat !=null && branchlong !=null && String.valueOf(branchkilocount) !=null &&
                                                        serviceshldr !=null && branchmainid != null && servicefee != null) {
                                                    //---------------------------------Checking Laundry Shops nearby-----------------------------//
                                                        if(latitude != null && longitude != null) {
                                                        lat1 = Double.valueOf(latitude);
                                                        lon1 = Double.valueOf(longitude);

                                                        lat2 = branchlat;
                                                        lon2 = branchlong;

                                                        lat1Rad = Math.toRadians(lat1);
                                                        lat2Rad = Math.toRadians(lat2);

                                                        deltaLonRad = Math.toRadians(lon2 - lon1);
                                                        dist = Math.acos(Math.sin(lat1Rad) * Math.sin(lat2Rad) + Math.cos(lat1Rad) * Math.cos(lat2Rad) * Math.cos(deltaLonRad)) * EARTH_RADIUS_KM;
                                                        String strDistance = Double.toString(dist)+" Kilometers";
                                                    }
                                                    //---------------------------------Check if Laundry Shop is active-----------------------------//
                                                    if(branchstatus.equals("Active") && dist < 8){
                                                        branchnamearr.add(laundryname);
                                                        branchinfoarr.add(laundryname + "/" + branchaddress + "/" + branchcontactno + "/" + branchcontactperson
                                                                + "/" + branchkilocount + "/" + branchmainid + "/" + branchsnapshot.getKey()  + "/" + servicefee);
                                                        servicesarr.add(serviceshldr);

                                                        mslaundryshop.setItems(branchnamearr);
                                                        mslaundryshop.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                                                            @Override
                                                            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                                                                if(position == 0){
                                                                    tvlaundryinfo.setText("No Laundry Shop selected");
                                                                    tvmenu.setText("No Laundry Shop selected");
                                                                }else{
                                                                    GetLaundryShopInformation(position);
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
            return null;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        String orderstat = prefs.getString("bookstatus", "Status");
        if(orderstat.equals("searching")){
            RiderFindStatus();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1000) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    public void GetLaundryShopInformation(int position){
        SpannableStringBuilder infobuilder = new SpannableStringBuilder();
        SpannableStringBuilder servicebuilder = new SpannableStringBuilder();
        String[] servicetokens;
        SpannableString str0, str1, str2, str3,str4;
        SpannableString straddress, straddressval, strcontact, strcontactval;

        serviceprovided = new ArrayList<>();

        String branchinfo = branchinfoarr.get(position-1);
        serviceprovided = servicesarr.get(position-1);

        tvlaundryinfo.setText(" ");
        tvmenu.setText(" ");

        String stringtoparse = branchinfo;
        String parser = stringtoparse;
        String delims = "[/]+";

        String[] tokens = parser.split(delims);
        Lname = String.valueOf(tokens[0]);
        Laddress = String.valueOf(tokens[1]);
        Lcontactnumber = String.valueOf(tokens[2]);
        Lcontactperson = String.valueOf(tokens[3]);
        Lkilocount = String.valueOf(tokens[4]);
        Lmainbranchid = String.valueOf(tokens[5]);
        Lbranchid = String.valueOf(tokens[6]);
        Lservicefee = String.valueOf(tokens[7]);

        //---------------------------------Laundry Basic Information-----------------------------//
        tvlaundryinfoheader.setText("Shop Information");
        straddress= new SpannableString("Branch Address: ");
        straddress.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.textColor)), 0, straddress.length(), 0);
        infobuilder.append(straddress);

        straddressval= new SpannableString(Laddress + "\n");
        straddressval.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorPrimary)), 0, straddressval.length(), 0);
        infobuilder.append(straddressval);

        strcontact= new SpannableString("Branch Contact #: ");
        strcontact.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.textColor)), 0, strcontact.length(), 0);
        infobuilder.append(strcontact);

        strcontactval= new SpannableString(Lcontactnumber);
        strcontactval.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorPrimary)), 0, strcontactval.length(), 0);
        infobuilder.append(strcontactval);

        tvlaundryinfo.setText(infobuilder, TextView.BufferType.SPANNABLE);
        //---------------------------------Laundry Services Offered--------------------------------//
        str0= new SpannableString("*Minimum of "+Lkilocount+"kg (General Clothes)"+"\n");
        str0.setSpan(new StyleSpan(Typeface.BOLD), 0, str0.length(), 0);
        str0.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.textColor)), 0, str0.length(), 0);
        servicebuilder.append(str0);

        for(int x = 0;x<serviceprovided.size();x++){
            stringtoparse = serviceprovided.get(x);
            parser = stringtoparse;
            delims = "[/]+";
            servicetokens = parser.split(delims);

            str1= new SpannableString(servicetokens[0]+": ");
            str1.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.textColor)), 0, str1.length(), 0);
            servicebuilder.append(str1);

            str2= new SpannableString("₱"+servicetokens[1]+"\n");
            str2.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorPrimary)), 0, str2.length(), 0);
            servicebuilder.append(str2);
        }

        str3= new SpannableString("Service Charge: ");
        str3.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.textColor)), 0, str3.length(), 0);
        servicebuilder.append(str3);

        str4= new SpannableString("₱"+Lservicefee+"\n");
        str4.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorPrimary)), 0, str4.length(), 0);
        servicebuilder.append(str4);
        tvmenu.setText(servicebuilder, TextView.BufferType.SPANNABLE);
    }

    public void RetrieveUserData() {
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_TYPE, MODE_PRIVATE);
        ordertransaction = prefs.getString("booktransaction", "order");
        uid = prefs.getString("userid", "UID");
        name = prefs.getString("username", "Name");
        email = prefs.getString("useremail", "Email");
        address = prefs.getString("useraddress", "Address");
        contact = prefs.getString("usercontact", "Contact");
    }

    protected void getLocation() {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        criteria = new Criteria();
        bestProvider = locationManager.getBestProvider(criteria,true);

        //You can still do this if you like, you might get lucky:
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        if (location != null) {
            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());
            geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(Double.valueOf(latitude), Double.valueOf(longitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                faddress = address;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void CheckGpsStatus(){
        locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        GPSStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void displayPromptForEnablingGPS(final Activity activity) {
        final AlertDialog.Builder builder =  new AlertDialog.Builder(activity);
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        final String message = "GPS/Location Setting must be enabled to continue";
        Toast.makeText(activity, "Location Settings is not On!", Toast.LENGTH_SHORT).show();
        builder.setMessage(message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                activity.startActivity(new Intent(action));
                                d.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });
        builder.create().show();
    }

    public void displayPromptForEnablingNetwork(final Activity activity, String message) {
        final AlertDialog.Builder builder =  new AlertDialog.Builder(activity);
        builder.setMessage(message)
                .setPositiveButton("Try Again",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                InitializeFunctions();
                                d.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });
        builder.create().show();
    }

    public boolean haveNetworkConnection(Activity activity) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)  activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
//                            Toast.makeText(getActivity(), "Success", Toast.LENGTH_LONG).show();
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                            Toast.makeText(getActivity(), "GPS is not on", Toast.LENGTH_LONG).show();
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(getActivity(), 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Toast.makeText(getActivity(), "Settings change not allowed", Toast.LENGTH_SHORT).show();
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }
}
